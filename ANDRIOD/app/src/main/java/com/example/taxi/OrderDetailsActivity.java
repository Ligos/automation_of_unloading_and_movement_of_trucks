package com.example.taxi;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import javax.xml.datatype.Duration;

public class OrderDetailsActivity extends AppCompatActivity {

    TextView nameTextView, originAddressTextView, destinationAddressTextView, deliveryTimeTextView,
            paymentTextView, phoneNumberTextView, doneStatusTextView, quantity_of_passengersTextView,
            presence_of_trunkTextView, deliveryDateTextViev, presence_of_child_seatTextView;
    Button changeStatusButton;

    private OrderDataModel receivedOrder;
    private String serverUrl;

    @SuppressLint("SetTextI18n")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.order_details_activity);
        getSupportActionBar().hide();

        // Assigning TextViews with IDs
        nameTextView = (TextView) findViewById(R.id.name);
        originAddressTextView = (TextView) findViewById(R.id.origin_address);
        destinationAddressTextView = (TextView) findViewById(R.id.destination_address);
        deliveryTimeTextView = (TextView) findViewById(R.id.delivery_time);
//        paymentTextView = (TextView) findViewById(R.id.payment);
//        quantity_of_passengersTextView = (TextView) findViewById(R.id.number_of_movers);
//        phoneNumberTextView = (TextView) findViewById(R.id.phone_number);
//        presence_of_trunkTextView = (TextView) findViewById(R.id.presence_of_trunk);
        deliveryDateTextViev = (TextView) findViewById(R.id.deliverydate);
//        presence_of_child_seatTextView = (TextView) findViewById(R.id.presence_of_child_seat);


        changeStatusButton = (Button) findViewById(R.id.change_status_button);
        doneStatusTextView = (TextView) findViewById(R.id.done_status);
        //customerImageView = (ImageView) findViewById(R.id.customer_image);


        //MARK: Receiving Value into activity using intent   //OrderDataModel TempHolder = (OrderDataModel) getIntent().getSerializableExtra("ClickedOrder");
        Intent intent = getIntent();
        receivedOrder = (OrderDataModel) intent.getExtras().getSerializable(MainActivity.CLICKED_ORDER_EXTRA);
        serverUrl = (String) intent.getStringExtra(MainActivity.SERVER_URL_EXTRA);


        //Dislaying Info in TextViews
        nameTextView.setText("Терминал:  " + receivedOrder.getCustomerName());
        originAddressTextView.setText("Откуда:  " + receivedOrder.getOriginAdress());
        destinationAddressTextView.setText("Куда:  " + receivedOrder.getDestinantionAddress());
        deliveryTimeTextView.setText("Время:  " + receivedOrder.getDeliveryTime());
        deliveryDateTextViev.setText("День:  " + receivedOrder.getDeliveryDate());
//        paymentTextView.setText("Сумма к оплате:  " + String.valueOf(Double.valueOf(receivedOrder.getPayment())) + "₱");
//        quantity_of_passengersTextView.setText("‍Кол-во пассажиров:  " + String.valueOf(receivedOrder.getQuantity_of_passengers()));
//        phoneNumberTextView.setText("Номер телефона заказчика:  " + receivedOrder.getPhoneNumber());
//        presence_of_trunkTextView.setText("Багаж: " + receivedOrder.getPresence_of_trunk());
//        presence_of_child_seatTextView.setText("Детское кресло: " + receivedOrder.getPresence_of_child_seat());


        Context context = getApplicationContext();


        redrawInterface();
    }

    public void didChangeStatusButtonClick(View view/*del?*/) {

        changeStatusButton.setAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.button_scale_animation));

        // MARK: Changing local instance status and redrawing interface
        if (receivedOrder != null) {

            // Changing local copy
            int statusId = receivedOrder.getStatus().ordinal() + 2; // 2 is server shift
            receivedOrder.setStatus(statusId+1);

            // Returning result to MainActivity ( <-- )
            Intent intent = new Intent();
            intent.putExtra(MainActivity.ORDER_ID_RESPONSE, receivedOrder.getId());
            intent.putExtra(MainActivity.NEW_STATUS_ID_RESPONSE, statusId + 1);
            setResult(RESULT_OK, intent);

            // updateUI
            redrawInterface();

            // TODO Changing status on the server (POST-request)
            changeOrderStatusOnServer(serverUrl, receivedOrder.getId(), statusId+1);
        }




    }

    private void redrawInterface(){
        switch (receivedOrder.getStatus()) {
            case DEFAULT:
                // Style for button
                changeStatusButton.setText("Начать исполнение");
                changeStatusButton.setBackgroundColor(Color.rgb(247, 233, 173));

                // TODO: change status to SEEN
//                    didChangeStatusButtonClick();
//                    dispatchQueue with delay 700ms
                AsyncStatusChanger changer = new AsyncStatusChanger();
                changer.execute();
                break;

            case SEEN:
                // Style for button
                changeStatusButton.setText("Начать исполнение");
                changeStatusButton.setBackgroundColor(Color.rgb(245, 194, 66));
                break;

            case STARTED:
                // Style for button
                changeStatusButton.setText("На месте!");
                changeStatusButton.setBackgroundColor(Color.rgb(255, 255, 102));
                break;

            case ARRIVED:
                // Style for button
                changeStatusButton.setText("Завершить заказ");
                changeStatusButton.setBackgroundColor(Color.rgb(72, 219, 136));
                break;

            case DONE:
                // Style for button
                changeStatusButton.setVisibility(View.GONE);
                doneStatusTextView.setVisibility(View.VISIBLE);
                break;

            default:
                Toast.makeText(this,"WRONG_STATUS", Toast.LENGTH_LONG).show();
        }
    }

    private void changeOrderStatusOnServer(String url, int orderId, int newStatusId) {

        final JSONObject requestBody = new JSONObject();

        try {
            requestBody.put("request_type", "change_status");
            requestBody.put("order_id", orderId);
            requestBody.put("new_status", newStatusId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, requestBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                if (!response.optString("status").equals("OK")) {
                    Toast.makeText(getApplicationContext(), "Server haven't accepted status ID!", Toast.LENGTH_LONG).show();
                }
            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });

        // add to request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(request);
    }

    //    ======= STATUS CHANGER =======
    class AsyncStatusChanger extends AsyncTask {

//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//        }

        @Override
        protected Object doInBackground(Object[] objects) {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException exception) {
                exception.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);

            assert (receivedOrder.getStatus() == OrderStatus.DEFAULT);

            int statusId = receivedOrder.getStatus().ordinal() + 2; // 2 is server shift
            receivedOrder.setStatus(statusId+1);
            redrawInterface();

            Intent intent = new Intent();
            intent.putExtra(MainActivity.ORDER_ID_RESPONSE, receivedOrder.getId());
            intent.putExtra(MainActivity.NEW_STATUS_ID_RESPONSE, statusId + 1);
            setResult(RESULT_OK, intent);

            changeOrderStatusOnServer(serverUrl, receivedOrder.getId(), statusId+1);
        }
    }
}
