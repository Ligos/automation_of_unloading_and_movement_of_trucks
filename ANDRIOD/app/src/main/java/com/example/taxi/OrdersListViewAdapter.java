package com.example.taxi;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import static android.provider.ContactsContract.CommonDataKinds.Website.URL;

public class OrdersListViewAdapter extends BaseAdapter {

    private Context context;
    private ArrayList <OrderDataModel> dataSet;
    private String url, keyword, driverId;

    private SwipeRefreshLayout swipeRefreshLayout;

    public OrdersListViewAdapter(Context context, ArrayList dataSet, SwipeRefreshLayout swipeRefreshLayout, String url, String keyword, String driverId) {
        this.context = context;
        this.dataSet = dataSet;

        this.url = url;
        this.keyword = keyword;
        this.driverId = driverId;
        this.swipeRefreshLayout = swipeRefreshLayout;
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    // MARK: required Methods
    @Override
    public int getCount() {
        return dataSet.size();
    }

    @Override
    public Object getItem(int i) {
        return dataSet.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;

        if (dataSet.size() < 0) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.order_cell_layout, null, true);
            return view;
        }

        if (view == null) {
            holder = new ViewHolder();

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            view = inflater.inflate(R.layout.order_cell_layout, null, true);

            holder.nameTextView = (TextView) view.findViewById(R.id.name);
            holder.addressTextView = (TextView) view.findViewById(R.id.address);
            holder.statusTextView =  (TextView) view.findViewById(R.id.status);
            holder.cellContainer = (RelativeLayout) view.findViewById(R.id.cell_container_relative_layout);

            view.setTag(holder);
        }
        else {
            holder = (ViewHolder) view.getTag();
        }

        // TextViews Setting
        holder.nameTextView.setText("Заказчик : " + dataSet.get(i).getCustomerName());
        holder.addressTextView.setText("Адрес : " + dataSet.get(i).getOriginAdress());

        switch (dataSet.get(i).getStatus()) {
            case DEFAULT: holder.statusTextView.setText("Принят"); break;
            case SEEN: holder.statusTextView.setText("Просмотрен"); break; // 3 просмотрел
            case STARTED: holder.statusTextView.setText("Выполняется"); break; // 4 выехал в начальную точку
            case ARRIVED: holder.statusTextView.setText("На месте"); break; // 5 прибыл в начальную точку
            case DONE: holder.statusTextView.setText("Выполнен");
                holder.statusTextView.setTextColor(ContextCompat.getColor(context, R.color.done_status_color));  //holder.statusTextView.setTextColor(context.getResources().getColor(R.color.done_status_color));
                break; // 6 заврешиил
            default: holder.statusTextView.setText("Ошибка");
                holder.statusTextView.setTextColor(ContextCompat.getColor(context, R.color.error_status_color));// LEGA (String.valueOf(dataSet.get(i).getStatus()));
        }






        //REFRESH
//        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                dataSet.clear();
//                fetchOrderListWithKeyWord();
//
//
//            }
//        });

        return view;
    }



    private class ViewHolder {
        protected TextView nameTextView, addressTextView, statusTextView;
        protected RelativeLayout cellContainer;
    }

}
