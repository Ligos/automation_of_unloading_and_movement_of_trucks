import React, {useState, useEffect} from 'react';
import axios from 'axios';

import List from './componets/List'
import Tasks1 from './componets/Tasks/Tasks1';
import Tasks2 from './componets/Tasks/Tasks2';
import Tasks3 from './componets/Tasks/Tasks3';
import Tasks4 from './componets/Tasks/Tasks4';
import Tasks6 from './componets/Tasks/Tasks6';
import Tasks5 from './componets/Tasks/Tasks5';




function App() {
 const [lists,setLists] = useState(null);
const [activeItem,setActiveItem] = useState({id: 1, name: "Заказы"});


useEffect(() => {
  axios
    .get('/lists')
    .then(({ data }) => {
      setLists(data);
    });
  }, []);




  return (
    <div className="dip">
      <div className="dip__sidebar">
        { lists ? (
        <List
        items={lists}
          onClickItem={item => {setActiveItem(item);
          }}
          activeItem={activeItem}
          />):(
            "Загрузка"
          )}
      </div>
      <div className="dip__tasks">
          {lists && activeItem && activeItem.id===1  && <Tasks1 list={activeItem} setActiveItem={setActiveItem} />}
          {lists && activeItem && activeItem.id===2 && <Tasks2 list={activeItem}/>}
          {lists && activeItem && activeItem.id===3 && <Tasks3 list={activeItem}/>}
          {lists && activeItem && activeItem.id===4 && <Tasks6 list={activeItem}/>}
          {lists && activeItem && activeItem.id===5 && <Tasks4 list={activeItem}/>}
          {lists && activeItem && activeItem.id===6 && <Tasks5 list={activeItem}/>}


      </div>
    </div>
  );
}

export default App;
