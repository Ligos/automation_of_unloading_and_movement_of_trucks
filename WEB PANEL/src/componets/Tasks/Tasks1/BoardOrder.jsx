import React from 'react';
import axios from "axios";


import "./Tasks1.scss";




function BoardOrder(props) {
  const  drop = e=>{
    console.log(props.Truckid)
      e.preventDefault();

    var  values={truckLicense: props.name,
                orderId:props.idOrder,
                currentDtm:'2020-11-02'}
      console.log(values);
      
    
    SendFirebaseMessage("Заказ","Назначен новый заказ");
     values && axios.post(`/assignOrder`,values)
            .then((response) => {
              console.log(response);
            })
            .catch((error)=>{
              console.log(error);
            })
            
            axios
                .get('/driver')
                .then(({ data }) => {
                  props.setDriver(data);
                });

            var TruckID={ truckId:props.Truckid};
                axios
                .post('/assignedOrders', TruckID)
                .then(({ data }) => {
                  props.setOrder(data);
                });

                axios
                .get('/unassignedOrders')
                .then(({ data }) => {
                    props.setNotDistrOrder(data);
                })

          props.setActiveItem({id: 1, name: "Заказы"});
                
  }
  const DragOver = e =>{

      e.preventDefault();
  }

  return (
    <div>
    <div className="board__title"><p>{ props.name ? (props.name) : ("Заказы") }</p></div>
      <div id={props.id||props.orderId}
           className={props.className}
           onDrop={drop}
           onDragOver={DragOver}
              >
          {props.children}
      </div>
      </div>
  )
}

function SendFirebaseMessage(title, body){
  //var Token = "c6KxWWnygIY:APA91bEU3ArlDKIlm_0939_ZXjjkLymFKUasT5yK8iHE5HNz5o9Ysql8FWpY7F2hQUW60uyI6AP23dUWsrcjd10_oSyA4LOd4u2NA-4eoGMkYh0i_vR9qBZ3vKnu13ObCWkmn04DEV7G"
  var Token = "eWqWXboCbpo:APA91bED4hit0mpwFXNorvEiQx-GtI0B99GfCqI0MBqACs4S3I8DugIV_XRIH2GNbrAwvXDuvtFF7_68t4R9lhT2tYRjBJrNqmqQm0seyynKAVxGNV5P2wizbMtEhdOe4A0gC_0sHGyN"
  var fcm_server_key = "AIzaSyBIVHBfM1qEoQDRD-SW1HpYbOrIyzfjlv4"

  axios({
      method: 'POST',
      url: 'https://fcm.googleapis.com/fcm/send',
      data: JSON.stringify(
               {
              "notification":{
                  "title": title,  //Любое значение
                  "body": body,  //Любое значение
                  "sound": "default", //Если вы хотите звучание в уведомление
                  "click_action": "FCM_PLUGIN_ACTIVITY",  //Должен присутствовать для Android
                  "icon": "fcm_push_icon"  //Белая иконка ресурса Android
                },
                "data":{
                  "param1":"value1",  //Любые данные, получаемые в callback - уведомлении
                  "param2": "Prueba"
                },
                "to": Token, //Тема(топик) или какое-то одно устройство
                "priority":"high", //Если не установлен, то уведомления не могут быть доставлены для закрытых приложений iOS
                "restricted_package_name":"" //Необязательно. Устанавливается для фильтрации приложений
              }
      ),
      headers: {
          'Content-Type': 'application/json', 'Authorization': 'key=' + fcm_server_key
        }
      }).then((response) => {
          console.log(response)
      })
      .catch((error) => {
          console.log(error)
      });
}
export default BoardOrder

