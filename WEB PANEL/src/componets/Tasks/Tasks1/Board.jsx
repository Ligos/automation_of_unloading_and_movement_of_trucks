import React from 'react';
import axios from "axios";


import "./Tasks1.scss";




function Board(props) {

  return (
    <div>
    <div className="board__title"><p>Список водителей</p></div>
      <div className={props.className} >
      {props.driver && props.driver.map((driver,index) => (
        

            
        <div 
          key={index}
          
          className="card">
      
          <p>{driver.firstName+" "+driver.lastName}</p>
      <p style={{fontSize: "11px"}}>Номер машины : {driver.licensePlateNumber}</p>
      <p>Вес груза : {driver ? (driver.sumWeightKg):("0")}/{driver.payLoadKg}</p>
      <p>Статус: {driver.statusName}</p>
          <p onClick={()=> {
          props.setIdTruck(driver.licensePlateNumber);
          props.setTruckid(driver.truckId);
          props.setDriverId(driver.driverId)
           axios
           .post('/assignedOrders', driver)
           .then(({ data }) => {
             props.setOrder(data);
           })}} className="visibleButtonListOrder">+ Список заказов </p>
          </div>
     
      
  ))
}
</div>
</div>
)
}
export default Board

