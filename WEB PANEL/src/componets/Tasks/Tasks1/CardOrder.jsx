import React from 'react';



import "./Tasks1.scss";
import axios from 'axios';


function CardOrder(props) {
   

    const DragStart = e=>{
        const target=e.target;
        props.setIdOrder && props.setIdOrder(target.id);
    }

    const DragOver = e =>{
        e.stopPropagation();
    }
    return (
        <div>
        {props.order && props.order.map((order,index) => (
        
        
           
        <div 
       style={{background:"#EBEBEB"}}
            id={order.id||props.orderId}
           key={index}
            className="card"
            draggable={props.draggable}
            onDragStart={DragStart}
            onDragOver={DragOver}
            
            >
            {props.children}
        <h6 className="button__delete" onClick={()=>{
            if (order.id){
                var values = {orderId : order.id};
                axios
                .post("/deleteOrder", values)
                .then(()=>{
                    axios
                        .get('/unassignedOrders')
                        .then(({ data }) => {
                            props.setNotDistrOrder(data);
                        });
                    })

            }
            else{
            axios
                .post("/cancelAssign", order)
                .then(()=>{
                    axios
                        .get('/unassignedOrders')
                        .then(({ data }) => {
                            props.setNotDistrOrder(data);
                        });

                    axios
                        .get('/driver')
                        .then(({ data }) => {
                          props.setDriver(data);
                        });
                    
                        var TruckID={ truckId:props.Truckid};
                        
                    axios
                            .post('/assignedOrders', TruckID)
                            .then(({ data }) => {
                            props.setOrder(data);
                            })
                    })
         } }}>X</h6>
        <p>Заказ : {order.orderName||order.name}</p>   
        <p>Номер терминала : {order.terminalId}</p>
        <p>Вес груза : {order.weightKg} кг</p>
        <p style={{fontSize: "10px",textOverflow: "ellipsis",whiteSpace: "nowrap"}}>Адреc_д: {order.destination}</p>
        </div>
        
    ))
}
</div>
    )
}

export default CardOrder





