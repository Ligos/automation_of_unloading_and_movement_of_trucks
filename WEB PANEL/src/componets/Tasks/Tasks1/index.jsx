import React, {useState, useEffect} from 'react';
import axios from 'axios';






import './Tasks1.scss';
import Board from './Board.jsx'
import BoardOrder from "./BoardOrder"
import CardOrder from './CardOrder';



const Tasks1 = ({list,setActiveItem}) => {
    const [order, setOrder] = useState(null);
    const [NotDistrOrder, setNotDistrOrder] = useState(null);
    const [visible, setVisible] = useState(false);
    const [idTruck, setIdTruck] = useState(null);
    const [Truckid, setTruckid] = useState(null);
    const [idOrder, setIdOrder] = useState(null);
    const [driver, setDriver] = useState(null);
    const [driverId, setDriverId] = useState(null);


    useEffect(() => {
        axios
          .get('/driver')
          .then(({ data }) => {
            setDriver(data);
          });
        }, []);
        
        useEffect(() => {
            axios
              .get('/unassignedOrders')
              .then(({ data }) => {
                setNotDistrOrder(data);
              });
            }, []);

    return (
        <div>
             <div className="tasks">
         <h2 className="tasks__title">{list.name}</h2>
         <div className='tasks__items'>
            <div className='flexbox'>
    <Board className="board" setIdTruck={setIdTruck} setTruckid={setTruckid&&setTruckid} visible={visible} driver={driver} setOrder={setOrder} setDriverId={setDriverId&&setDriverId} order={order}  setVisible={setVisible}></Board>
     <BoardOrder idOrder={idOrder} setNotDistrOrder={setNotDistrOrder&&setNotDistrOrder} setActiveItem={setActiveItem} setOrder={setOrder} Truckid={Truckid&&Truckid} name={idTruck&&idTruck} setDriver={setDriver} className="board" id={idTruck&&idTruck} > {order&&setNotDistrOrder&&<CardOrder order={order} setDriver={setDriver} setNotDistrOrder={setNotDistrOrder} Truckid={Truckid&&Truckid} setOrder={setOrder}  draggable="true" id="Card-2"></CardOrder>}</BoardOrder> 
    <BoardOrder  name="Нераспределенные заказы" className="board" id="board-2" ><CardOrder order={NotDistrOrder} setIdOrder={setIdOrder} setNotDistrOrder={setNotDistrOrder}  draggable="true" id="Card-3"></CardOrder></BoardOrder>
            
               
            </div>
            <div className='tasks__items-row'>
               
            </div>
         </div>
        </div>
        <button className="button"  onClick={() => {
        var values= {driverId: driverId}
        axios
        .post('/planner', values)
        .then(()=>{
          axios
              .get('/unassignedOrders')
              .then(({ data }) => {
                 setNotDistrOrder(data);
              });

          axios
              .get('/driver')
              .then(({ data }) => {
                setDriver(data);
              });
          
              var TruckID={ truckId:Truckid};
              
          axios
                  .post('/assignedOrders', TruckID)
                  .then(({ data }) => {
                  setOrder(data);
                  })
         })
         .catch(error =>{
           alert(error.response.data)
         })
        }
        }> Использовать планировщик </button>
        </div>
    )
   
}





export default Tasks1

