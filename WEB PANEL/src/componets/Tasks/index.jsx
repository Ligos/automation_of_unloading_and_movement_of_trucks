import React from 'react'

import './Tasks.scss';

   

const Tasks = ({list}) => {
    return (
        <div className="tasks">
         <h2 className="tasks__title">{list.name}</h2>
         <div className='tasks__items'>
            <div className='tasks__items-row'>
             {
               list.tasks.map(task => (
                <div key={task.id}>
                    <p>{task.text}</p>
                     <input id={`task-${task.id}`}  type="text" size="30"/> 
                </div>))       
             }
            </div>
         </div>
        </div>
    )
}

export default Tasks;
