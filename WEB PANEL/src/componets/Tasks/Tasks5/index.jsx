import React from 'react'

import './Tasks5.scss';
import sh from '../../../assets/img/shR.png'


   

const Tasks5 = ({list}) => {
    return (
        <div className="tasks">
         <h2 className="tasks__title">{list.name}</h2>
         <div className='tasks__items'>
            <img src={sh} alt="Схема" />
         </div>
        </div>
    )
}


export default Tasks5;
