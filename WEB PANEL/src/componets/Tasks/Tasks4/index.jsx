import React, {useEffect, useState}from 'react'
import axios from "axios"

import './Tasks4.scss';


   

const Tasks4 = ({list}) => {
    const [Terminal,setTerminals] = useState(null);
useEffect(() => {
  axios
    .get('/terminals')
    .then(({ data }) => {
      setTerminals(data);
    });
  }, []);
    return (
        <div className="tasks">
         <h2 className="tasks__title">{list.name}</h2>
         <div className='tasks__items'>
        
         {Terminal&&Terminal.map((Terminal,index) => (
             

            <div  key={index} className="statis">
                <ul> 
                <li style={{ textAlign: "center"}}><b>Терминал: {Terminal.terminalName} </b></li>
                <li>Число заявок в очереди: {Terminal.orderCurrentCnt}</li>
                <li>Среднее время выполнения заявки: {Terminal.avgLoadingTimeMin}</li>
                <li>Среднее время заявки в очереди: {Terminal.avgQueueTimeMin}</li>
                </ul>
              </div>
              
              
            ))
        }
        </div>

         </div>
        
    )
}


export default Tasks4;
