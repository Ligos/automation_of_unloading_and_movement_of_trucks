import React, { useState } from 'react';

import './Tasks6.scss';
import ParamTruck from './calculatorP.jsx/ParamTruck.jsx';
import TrailerParameters from './calculatorP.jsx/TrailerParameters.jsx';
import LoadСalculation from './calculatorP.jsx/LoadСalculation.jsx';

const valueTruck={
        Lt: 3600,
        Mt: 8180,
        NT1: 5726,
        NT2: 2454,
        l1: 3060,
        l2: 540
}
const valueTrailer={
        LA: 13620,
        LB: 7700,
        LC: 4260,
        Mp: 6400,
        NTP: 1280

}


function Greeting(props) {
  const [dataParamTruck, setdataParamTruck]=useState(valueTruck);
  const [dataTrailerParameters, setdataTrailerParameters]=useState(valueTrailer);
    const isLoggedIn = props.isLoggedIn;
    if (isLoggedIn===1) {
      return <ParamTruck setdataParamTruck={setdataParamTruck} />;
    }
    if (isLoggedIn===2) {
        return <TrailerParameters setdataTrailerParameters={setdataTrailerParameters} />;
      }
      if (isLoggedIn===3) {
        return <LoadСalculation dataParamTruck={dataParamTruck} dataTrailerParameters={dataTrailerParameters} />;
      }
    }
   


class calculator extends React.Component{
    constructor(props) {
        super(props);
        this.handleParamTruckClick = this.handleParamTruckClick.bind(this);
        this.handleTrailerParametersClick = this.handleTrailerParametersClick.bind(this);
        this.handleLoadСalculationClick = this.handleLoadСalculationClick.bind(this);
        this.state = {isLoggedIn: 3};
      }
    
      handleParamTruckClick() {
        this.setState({isLoggedIn: 1});
      }
    
      handleTrailerParametersClick() {
        this.setState({isLoggedIn: 2});
      }

      handleLoadСalculationClick() {
        this.setState({isLoggedIn: 3});
      }
    
      render() {
        const isLoggedIn = this.state.isLoggedIn;
        return (
          <div className="GroupButton">
           <div className="divGroup"> <button className="button-s"  onClick={this.handleParamTruckClick}>Параметры тягача</button></div>
           <div className="divGroup"><button className="button-s" onClick={this.handleTrailerParametersClick}>Параметры полуприцепа</button></div>
           <div className="divGroup"><button className="button-s" onClick={this.handleLoadСalculationClick}>Расчет нагрузок на оси</button></div>         
            <Greeting isLoggedIn={isLoggedIn} />
          </div>
        );
      }
      
    
}

export default calculator;