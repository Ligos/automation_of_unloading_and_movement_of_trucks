import React from 'react';
import { useFormik } from 'formik';
import '../Tasks6.scss';



function TrailerParameters(props) {
    const validate = values => {
        const errors = {};
        if (!values.Mp) {
          errors.Mp = 'Required';
        } 
      
        if (!values.LB) {
          errors.LB = 'Required';
        }
       

        if (!values.LA) {
            errors.LA = 'Required';
          }
        
          if (!values.LC) {
            errors.LC = 'Required';
          }
          
          if (!values.NTP) {
            errors.NTP = 'Required';
          }
    };
    
    const formik = useFormik({
        initialValues: {
          Mp: 6400,
          LB: 7700,
          LA: 13620,
          LC: 4260,
          NTP: 1280,

        },
        validate,
        onSubmit: values => {
          alert(JSON.stringify(values, null, 2));
          props.setdataTrailerParameters(values)
        }
    
      });
    return (
    <div>
        <form onSubmit={formik.handleSubmit}>
     <div className="tasks__form-fields">
     <div className="divfield">
             <label htmlFor="Mp">Масса пустого полуприцепа, кг:</label>
            <input
                    id="Mp"
                    name="Mp"
                    type="text"
                    className="field"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.Mp}
                />
                {formik.touched.Mp && formik.errors.Mp ? (
                    <div>{formik.errors.Mp}</div>
                     ) : null}
            </div>
            <div  className="divfield">
                <label htmlFor="LB">Расстояние от средней оси до точки сцепки полуприцепа, мм:</label>
                <input
                        id="LB"
                        name="LB"
                        type="text"
                        className="field"
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.LB}
                    />
                    {formik.touched.LB && formik.errors.LB ? (
                        <div>{formik.errors.LB}</div>
                        ) : null}
                     
            </div>
            <div  className="divfield">
                <label htmlFor="LA">Погрузочная (полезная) длинна полуприцепа, мм: </label>
                <input
                        id="LA"
                        name="LA"
                        type="text"
                        className="field"
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.LA}
                    />
                    {formik.touched.LA && formik.errors.LA ? (
                        <div>{formik.errors.LA}</div>
                        ) : null}
                     
            </div>
            <div  className="divfield">
                <label htmlFor="LC">Расстояние от средней оси до задней стенки полуприцепа, мм: </label>
                <input
                        id="LC"
                        name="LC"
                        type="text"
                        className="field"
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.LC}
                    />
                    {formik.touched.LC && formik.errors.LC ? (
                        <div>{formik.errors.LC}</div>
                        ) : null}
                     
            </div>
            <div  className="divfield">
                <label htmlFor="NTP">Нагрузка пустого полуприцепа на тягач, кг:  </label>
                <input
                        id="NTP"
                        name="NTP"
                        type="text"
                        className="field"
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.NTP}
                    />
                    {formik.touched.NTP && formik.errors.NTP ? (
                        <div>{formik.errors.NTP}</div>
                        ) : null}
                     
            </div>

    </div>
    <button className="button" type="submit"> Продолжить </button>
     </form></div>)
  }

  export default TrailerParameters;