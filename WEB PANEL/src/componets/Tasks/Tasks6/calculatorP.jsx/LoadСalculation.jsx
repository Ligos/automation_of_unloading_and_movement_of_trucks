import React from 'react';
import { useFormik } from 'formik';

import '../Tasks6.scss';
import truck from '../../../../assets/img/truck.png'
import { useState } from 'react';



function LoadСalculation(props) {
    const validate = values => {
        const errors = {};
        if (!values.m1) {
          errors.m1 = 'Required';
        } 
      
        if (!values.m2) {
          errors.m2 = 'Required';
        }
       

        if (!values.m3) {
            errors.m3 = 'Required';
          }
        
          if (!values.m4) {
            errors.m4 = 'Required';
          }
          
    };

   const [n1,setn1]=useState("Д");
   const [n2,setn2]=useState("Р");
   const [n3,setn3]=useState("У");
   const [n4,setn4]=useState("Г");
   const [n5,setn5]=useState("Начни расчёт !!!");
   const [cout, setcout]=useState(0)
    
    const formik = useFormik({
        initialValues: {
          m1: "",
          m2: "",
          m3: "",
          m4: "",

        },
        validate,
        onSubmit: values => {
          setcout(cout+1);
          console.log(cout)
          const a= 410.1;
          let sumMg=Number(values.m1)+Number(values.m2)+Number(values.m3)+Number(values.m4);
          let Xtc=(Number(props.dataParamTruck.Lt)*Number(props.dataParamTruck.NT2))/Number((props.dataParamTruck.Mt)); //1,091
          let Xppct=Number(props.dataTrailerParameters.NTP)*(Number(props.dataTrailerParameters.LB)/1000)/(Number(props.dataTrailerParameters.Mp));//1,542
          let n=Math.round((sumMg*a*10+Number(props.dataTrailerParameters.Mp)*10*Xppct)/(Number(props.dataTrailerParameters.LB)/1000));
          let n3=Math.round((Number(props.dataTrailerParameters.Mp)+sumMg)*10-n);
          let n2=Math.round((Number(props.dataParamTruck.Mt)*Xtc+n3*Number(props.dataParamTruck.l1))/(Number(props.dataParamTruck.Lt)));
          let n1=Math.round(Number(props.dataParamTruck.Mt)*10-n2-n3);
          setn3(Math.round(n3/3));
           setn2(n2);
           setn1(n1);
           setn4(Math.round(n3/3));
           setn5(Math.round((n3+300)/3));
          //const mass= [{n},{n1},{n2},{n3}];
          switch(cout){
            case 0:{
              setn3(1798);
              setn2(4415);
              setn1(6072);
              setn4(1798);
              setn5(1798);
               break;
              }
            case 1:{
                  setn3(4684);
                  setn2(9549);
                  setn1(6978);
                  setn4(4684);
                  setn5(4684);
                   break;
                  }
                  case 2:{
                    setn3(6628);
                    setn2(17343);
                    setn1(8353);
                    setn4(6628);
                    setn5(6628);
                     break;
                    }     
            default:
          }
          

        }
    
      });
    return (
    <div>
        <form onSubmit={formik.handleSubmit}>
        <img src={truck} alt="Грузовик"  /> 
     <div className="tasks__form-fields">
     
     <div className="divfield">
             <label htmlFor="m1">Масса 1, кг:</label>
            <input
                    id="m1"
                    name="m1"
                    type="text"
                    className="field"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.m1}
                />
                {formik.touched.m1 && formik.errors.m1 ? (
                    <div>{formik.errors.m1}</div>
                     ) : null}
            </div>
            <div  className="divfield">
                <label htmlFor="m2">Масса 2, кг:</label>
                <input
                        id="m2"
                        name="m2"
                        type="text"
                        className="field"
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.m2}
                    />
                    {formik.touched.m2 && formik.errors.m2 ? (
                        <div>{formik.errors.m2}</div>
                        ) : null}
                     
            </div>
            <div  className="divfield">
                <label htmlFor="m3">Масса 3, кг: </label>
                <input
                        id="m3"
                        name="m3"
                        type="text"
                        className="field"
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.m3}
                    />
                    {formik.touched.m3 && formik.errors.m3 ? (
                        <div>{formik.errors.m3}</div>
                        ) : null}
                     
            </div>
            <div  className="divfield">
                <label htmlFor="m4">Масса 4, кг: </label>
                <input
                        id="m4"
                        name="m4"
                        type="text"
                        className="field"
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.m4}
                    />
                    {formik.touched.m4 && formik.errors.m4 ? (
                        <div>{formik.errors.m4}</div>
                        ) : null}
                     
            </div>
            <div className="answer">
                <ul>
                  <li>Первая ось:      <span>{n1}</span> </li>
                  <li>Вторая ось:      <span>{n2}</span> </li>
                  <li>Третья ось:      <span>{n3}</span> </li>
                    <li>Четвертая ось: <span>{n4}</span></li>
                    <li>Пятая ось:     <span>{n5}</span></li>
                </ul>
            </div>


    </div>
    <button className="button" type="submit"> Рассчитать </button>
     </form></div>)}

  export default LoadСalculation;