import React from 'react';
import { useFormik } from 'formik';
import '../Tasks6.scss';



function ParamTruck(props) {
    const validate = values => {
        const errors = {};
        if (!values.Mt) {
          errors.Mt = 'Required';
        } 
      
        if (!values.Lt) {
          errors.Lt = 'Required';
        }
       

        if (!values.l2) {
            errors.l2 = 'Required';
          }
        
          if (!values.l1) {
            errors.l1 = 'Required';
          }
          
          if (!values.NT2) {
            errors.NT2 = 'Required';
          }
          
          if (!values.NT1) {
            errors.NT1 = 'Required';
          }
          return errors;
    };
    
    const formik = useFormik({
        initialValues: {
          Mt: 8180,
          Lt: 3600,
          l2: 540,
          l1: 3060,
          NT2: 2454,
          NT1: 5726,

        },
        validate,
        onSubmit: values => {
          alert(JSON.stringify(values, null, 2));
          props.setdataParamTruck(values);
        }
    
      });
    return (
    <div>
        <form onSubmit={formik.handleSubmit}>
     <div className="tasks__form-fields">
     <div className="divfield">
             <label htmlFor="Mt">Масса тягача, кг:</label>
            <input
                    id="Mt"
                    name="Mt"
                    type="text"
                    className="field"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.Mt}
                />
                {formik.touched.Mt && formik.errors.Mt ? (
                    <div>{formik.errors.Mt}</div>
                     ) : null}
            </div>
            <div  className="divfield">
                <label htmlFor="Lt">Расстояние между осями тягача, мм:</label>
                <input
                        id="Lt"
                        name="Lt"
                        type="text"
                        className="field"
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.Lt}
                    />
                    {formik.touched.Lt && formik.errors.Lt ? (
                        <div>{formik.errors.Lt}</div>
                        ) : null}
                     
            </div>
            <div  className="divfield">
                <label htmlFor="l2">Расстояние от задней оси до точки сцепки тягача, мм*: </label>
                <input
                        id="l2"
                        name="l2"
                        type="text"
                        className="field"
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.l2}
                    />
                    {formik.touched.l2 && formik.errors.l2 ? (
                        <div>{formik.errors.l2}</div>
                        ) : null}
                     
            </div>
            <div  className="divfield">
                <label htmlFor="l1">Расстояние от передней оси до точки сцепки тягача, мм*: </label>
                <input
                        id="l1"
                        name="l1"
                        type="text"
                        className="field"
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.l1}
                    />
                    {formik.touched.l1 && formik.errors.l1 ? (
                        <div>{formik.errors.l1}</div>
                        ) : null}
                     
            </div>
            <div  className="divfield">
                <label htmlFor="NT2">Нагрузка на заднюю ось пустого тягача, кг:  </label>
                <input
                        id="NT2"
                        name="NT2"
                        type="text"
                        className="field"
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.NT2}
                    />
                    {formik.touched.NT2 && formik.errors.NT2 ? (
                        <div>{formik.errors.NT2}</div>
                        ) : null}
                     
            </div>

            <div  className="divfield">
                <label htmlFor="NT1">Нагрузка на переднюю ось пустого тягача, кг:  </label>
                <input
                        id="NT1"
                        name="NT1"
                        type="text"
                        className="field"
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.NT1}
                    />
                    {formik.touched.NT1 && formik.errors.NT1 ? (
                        <div>{formik.errors.NT1}</div>
                        ) : null}
                     
            </div>
    </div>
    <button className="button" type="submit"> Продолжить </button>
     </form></div>)
  }

  export default ParamTruck;