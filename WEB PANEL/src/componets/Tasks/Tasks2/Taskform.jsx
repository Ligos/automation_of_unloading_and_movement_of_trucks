import React, { useState, useEffect } from 'react'
import { useFormik } from 'formik';
import axios from "axios";


import './Tasks2.scss';




function Taskform() {
  const [select, setSelect]=useState(null);
  const [avtoSelect, setAvtoSelect]=useState(null);

        const [selectType, setSelectType]=useState(null);
        useEffect(()=>{
        axios 
              .get('/terminalSelectType')
              .then(({data}) => {
                setSelectType(data);
              });        
            },[]);
   

    const validate = values => {
        const errors = {};
        if (!values.orderName) {
          errors.orderName = 'Required';
        } 
      
        if (!values.terminalType) {
          errors.terminalType = 'Required';
        }

        if (!values.weightKg) {
          errors.weightKg = 'Required';
        }
        
        if (!values.lengthMet) {
          errors.lengthMet = 'Required';
        }
        
        if (!values.widthMet) {
          errors.widthMet = 'Required';
        }
        if (!values.heightMet) {
          errors.heightMet = 'Required';
        }
        if (!values.cityDestination) {
          errors.cityDestination = 'Required';
        }
        if (!values.addressDestination) {
          errors.addressDestination = 'Required';
        }
        return errors;
    };

    const  formik = useFormik({
        initialValues: {
          orderName: '',
          terminalType: '',
          terminalName: '',
          weightKg: '',
          lengthMet: '',
          widthMet: '',
          heightMet: '',
          cityDestination: '',
          addressDestination:'',
        },
        validate,
         onSubmit: values => {
          if (!values.terminalName)
          {
              values.terminalName=avtoSelect.terminalName;
          }
          
          alert(JSON.stringify(values, null, 2));
          axios.post('/addorder', values)
            .then((response) => {
              console.log(response);
            })
            .catch((error) => {
              console.log(error);
  });
        }
 
      });
    return (

        <div className="tasks__form-button">
            
            <form onSubmit={formik.handleSubmit}>
            <div className="tasks__form-fields">
            <div className="divfield">
                    <label htmlFor="orderName">Имя заказа</label>
                    <input
                            id="orderName"
                            name="orderName"
                            type="text"
                            className="field"
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            value={formik.values.orderName}
                        />
                        {formik.touched.orderName && formik.errors.orderName ? (
                            <div>{formik.errors.orderName}</div>
                             ) : null}
                    </div>

                    <div  className="divfield">
                    <label  htmlFor="terminalType">Тип терминала: </label>
                    <select
                   
                    className="field"
                    id="terminalType"
                    name="terminalType"
                    onClick={()=>{
                     var valueType = {type: formik.values.terminalType }
                      axios 
                      .post('/terminalSelect', valueType)
                      .then(({data}) => {
                        setSelect(data);
                      });
                      axios 
                      .post('/avtoTerminalSelect', valueType)
                       .then(({data}) => {
                        setAvtoSelect(data);
                      });  
                    }}
                    value={formik.values.terminalType}
                    onChange={formik.handleChange}
                     >
                    <option  value="">Выберите тип</option>
                     {selectType && selectType.map((selectType,index) => ( 
                    <option
                     key={index}   value={selectType.type}>{selectType.type}</option>
                     ))}
                    </select>
                    {formik.touched.terminalType && formik.errors.terminalType ? (
                            <div>{formik.errors.terminalType}</div>
                             ) : null}
                    </div>

                    <div  className="divfield">
                    <label  htmlFor="terminalName">Терминал: </label>
                    <select
                    className="field"
                    id="terminalName"
                    name="terminalName"
                    value={formik.values.terminalName}
                    onChange={formik.handleChange}
                     >
                    <option  value="">Выберите термминла</option>
                     {select && select.map((select,index) => ( 
                    <option key={index}   value={select.name}>{select.name}</option>
                     ))}
                    </select>
                    </div>

                    <div  className="divfield">
                    <label htmlFor="weightKg">Вес, кг:</label>
                    <input
                            id="weightKg"
                            name="weightKg"
                            type="text"
                            className="field"
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            value={formik.values.weightKg}
                        />
                        {formik.touched.weightKg && formik.errors.weightKg ? (
                            <div>{formik.errors.weightKg}</div>
                             ) : null}
                    </div>

                    <div  className="divfield">
                    <label htmlFor="lengthMet">Длина, мм:</label>
                    <input
                            id="lengthMet"
                            name="lengthMet"
                            type="text"
                            className="field"
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            value={formik.values.lengthMet}
                        />
                        {formik.touched.lengthMet && formik.errors.lengthMet ? (
                            <div>{formik.errors.lengthMet}</div>
                             ) : null}
                    </div>

                    <div  className="divfield">
                    <label htmlFor="widthMet">Ширина, мм:</label>
                    <input
                            id="widthMet"
                            name="widthMet"
                            type="text"
                            className="field"
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            value={formik.values.widthMet}
                        />
                        {formik.touched.widthMet && formik.errors.widthMet ? (
                            <div>{formik.errors.widthMet}</div>
                             ) : null}
                    </div>

                    <div  className="divfield">
                    <label htmlFor="heightMet">Высота, мм:</label>
                    <input
                            id="heightMet"
                            name="heightMet"
                            type="text"
                            className="field"
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            value={formik.values.heightMet}
                        />
                        {formik.touched.heightMet && formik.errors.heightMet ? (
                            <div>{formik.errors.heightMet}</div>
                             ) : null}
                    </div>

                    <div  className="divfield">
                    <label htmlFor="cityDestination">Город</label>
                    <input
                            id="cityDestination"
                            name="cityDestination"
                            type="text"
                            className="field"
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            value={formik.values.cityDestination}
                        />
                        {formik.touched.cityDestination && formik.errors.cityDestination ? (
                            <div>{formik.errors.cityDestination}</div>
                             ) : null}
                    </div>

                    <div  className="divfield">
                    <label htmlFor="addressDestination">Улица</label>
                    <input
                            id="addressDestination"
                            name="addressDestination"
                            type="text"
                            className="field"
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            value={formik.values.addressDestination}
                        />
                        {formik.touched.addressDestination && formik.errors.addressDestination ? (
                            <div>{formik.errors.addressDestination}</div>
                             ) : null}
                    </div>

            </div>
            <button className="button" type="submit"> Добавить заказ </button>
            </form>
             
               
        </div>
    )
}

export default Taskform
