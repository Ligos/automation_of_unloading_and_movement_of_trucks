import React from 'react';
import './Timeform.scss';

function Timeform() {
    return (
        <div>
            <div className="block">
                <section>
                <div>
                    <p>09.30</p>
                </div>
                <div >
                    <p>10.00</p>
                </div>        
                <div >
                    <p>10.30</p>
                </div>
                <div >
                    <p>11.00</p>
                </div>
                <div >
                    <p>11.30</p>
                </div>
                <div >
                    <p>12.00</p>
                </div>
                <div >
                    <p>13.00</p>
                </div>
                <div >
                    <p>13.30</p>
                </div>
                <div >
                    <p>14.00</p>
                </div>
                <div >
                    <p>15.00</p>
                </div>
                <div >
                    <p>15.30</p>
                </div>
                <div >
                    <p>16.00</p>
                </div>
                <div >
                    <p>16.30</p>
                </div>
                <div >
                    <p>17.00</p>
                </div>
                <div >
                    <p>17.30</p>
                </div>
                <div >
                    <p>18.00</p>
                </div>
            
                </section>
            </div>
            
        </div>
    )
}

export default Timeform
