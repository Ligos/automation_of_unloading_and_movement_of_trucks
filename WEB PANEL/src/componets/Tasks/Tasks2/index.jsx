import React from 'react';

import './Tasks2.scss';
import Taskform from './Taskform';
//import Calculator from './calculator';



const Tasks2 = ({list}) => {
    return (
        <div>
             <div className="tasks">
         <h2 className="tasks__title">{list.name}</h2>
         <div className='tasks__items'>
            <Taskform/>
         
            <div className='tasks__items-row'>
               
            </div>
         </div>
        </div>
        </div>
    )
}


export default Tasks2

