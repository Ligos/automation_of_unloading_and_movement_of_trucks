import React, {useState, useEffect} from 'react'
import { useFormik } from 'formik';
import axios from "axios";

import './Tasks3.scss';



const Tasks3 = ({list}) => {

  const [licensePlateNumber, setlicensePlateNumber]=useState(null);
    useEffect(()=>{
    axios 
          .get('/licensePlateNumber')
          .then(({data}) => {
            setlicensePlateNumber(data);
          });        
        },[]);

    const validate = values => {
        const errors = {};
        if (!values.firstName) {
          errors.firstName = 'Required';
        } 
      
        if (!values.lastName) {
          errors.lastName = 'Required';
        }
      
        if (!values.fathersName) {
          errors.fathersName = 'Required';
        } 
        // if (!values.birthday) {
        //     errors.birthday = 'Required';
        //   } 
          if (!values.phoneNumber) {
            errors.phoneNumber = 'Required';
          } 
          if (!values.truckLicense) {
            errors.truckLicense = 'Required';
          } 
      
        return errors;
      };

    const formik = useFormik({
        initialValues: {
          firstName: '',
          lastName: '',
          fathersName: '',  
          // birthday: '',
          phoneNumber: '',
          truckLicense: '',
        },
        validate,
        onSubmit: values => {
          alert(values);
        
          axios.post('/adduser', values)
            .then((response) => {
              console.log(response);
            })
            .catch((error) => {
              console.log(error);
  });
        }
 
      });
    
    return (
        <div>
             <div className="tasks">
                <h2 className="tasks__title">{list.name}</h2>
                <form onSubmit={formik.handleSubmit}>
                <div className='tasks__items'>
                
                   
                    <div className='tasks__items-row'>
                    <div className="divfield">
                    <label htmlFor="firstName">Имя</label>
                    <input
                            id="firstName"
                            name="firstName"
                            type="text"
                            className="field"
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            value={formik.values.firstName}
                        />
                        {formik.touched.firstName && formik.errors.firstName ? (
                            <div>{formik.errors.firstName}</div>
                             ) : null}
                    </div>
                    <div  className="divfield">
                    <label htmlFor="lastName">Фамилия</label>
                    <input
                            id="lastName"
                            name="lastName"
                            type="text"
                            className="field"
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            value={formik.values.lastName}
                        />
                        {formik.touched.lastName && formik.errors.lastName ? (
                            <div>{formik.errors.lastName}</div>
                             ) : null}
                    </div>
                    <div  className="divfield">
                    <label htmlFor="fathersName">Отчетсво</label>
                    <input
                            id="fathersName"
                            name="fathersName"
                            type="text"
                            className="field"
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            value={formik.values.fathersName}
                        />
                         {formik.touched.fathersName && formik.errors.fathersName ? (
                            <div>{formik.errors.fathersName}</div>
                             ) : null}
                    </div>

                    {/* <div  className="divfield">
                    <label htmlFor="birthday">Дата рождения</label>
                    <input
                            id="birthday"
                            name="birthday"
                            type="text"
                            className="field"
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            value={formik.values.birthday}
                        />
                        {formik.touched.birthday && formik.errors.birthday ? (
                            <div>{formik.errors.birthday}</div>
                             ) : null}
                    </div> */}
                    
                    <div  className="divfield">
                      <label htmlFor="phoneNumber">Номер телефона</label>
                      <input
                              id="phoneNumber"
                              name="phoneNumber"
                              type="text"
                              className="field"
                              onChange={formik.handleChange}
                              onBlur={formik.handleBlur}
                              value={formik.values.phoneNumber}
                          />
                          {formik.touched.phoneNumber && formik.errors.phoneNumber ? (
                              <div>{formik.errors.phoneNumber}</div>
                              ) : null}
                    </div>
                   
                    <div  className="divfield">
                    <label  htmlFor="truckLicense">Номерные знаки грузовика: </label>
                    <select
                    className="field"
                    id="truckLicense"
                    name="truckLicense"
                    value={formik.values.truckLicense}
                    onChange={formik.handleChange}
                     >
                    <option   value="">Номерные знаки</option>
                     {licensePlateNumber && licensePlateNumber.map((licensePlateNumber,index) => ( 
                    <option key={index}   value={licensePlateNumber.licensePlateNumber}>{licensePlateNumber.licensePlateNumber}</option>
                     ))}
                    </select>
                    {formik.touched.truckLicense && formik.errors.truckLicense ? (
                            <div>{formik.errors.truckLicense}</div>
                             ) : null}
                    </div>
                       
                </div>    
                </div>
                <button className="button" type="submit"> Добавить водителя </button>
                </form>
             </div>
            
        </div>
    )
}


export default Tasks3
