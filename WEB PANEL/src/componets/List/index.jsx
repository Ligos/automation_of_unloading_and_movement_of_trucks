import React from 'react';

import classNames from 'classnames';
import './List.scss';

const List = ({ items, onClickItem, activeItem }) => {
    return(
    <ul className="list">
        {items.map((item, index) => (
        <li key={index}
            className={classNames(item.className, {active: activeItem  && activeItem.id===item.id })}
            onClick={onClickItem ? () => onClickItem(item): null}>  
            <span>{item.name}</span>
        </li> ))
        }
    </ul>
    )
}

export default List;