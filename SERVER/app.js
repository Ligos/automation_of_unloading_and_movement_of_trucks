const express = require('express');
const config = require('config');
const mysql = require('mysql2');
const dbWebPanel = require('./modules/dbWebPanel');
const dbAndroid = require('./modules/dbAndroid');


const PORT = config.get('port');
const URI = config.get('uri');

const DB_HOST = config.get('dbHost');
const DB_NAME = config.get('dbName');
const DB_USER_NAME = config.get('dbUserName');
const DB_USER_PASSWORD = config.get('dbUserPassword');
const DB_LIMIT_CONNECTION = config.get('dbLimitConnection');

const SIDEBAR_LIST = config.get('lists');

const STATUS_FAILURE = 400;


const json = express.json();
const app = express();


const connection = mysql.createPool({
    connectionLimit: DB_LIMIT_CONNECTION,
    host: DB_HOST,
    database: DB_NAME,
    user: DB_USER_NAME,
    password: DB_USER_PASSWORD
}).promise();

app.set('Access-Control-Allow-Origin', '*');

app.get('/lists', function(request, response){
    response.send(JSON.stringify(SIDEBAR_LIST));
});

app.get('/driver', function(request, response){
    connection.query("SELECT\
                    vdr.*\
                    , SUM(vao.weightKg) AS sumWeightKg\
                    , vtr.payLoadKg\
                    , vst.statusName\
                FROM vDrivers AS vdr\
                LEFT JOIN tOrderTransporation AS tot\
                    ON tot.truckId = vdr.truckId\
                LEFT JOIN vAssignedOrders AS vao\
                    ON vao.orderId = tot.orderId\
                INNER JOIN vtrucks AS vtr\
                    ON vdr.truckId = vtr.truckId\
                INNER JOIN vStatus AS vst\
					ON vst.statusId = vdr.statusId\
                GROUP BY\
                    vdr.driverID, vdr.login, vdr.firstName, vdr.lastName, vdr.fathersName, vdr.phoneNumber, vdr.statusID, vdr.truckId, vdr.licensePlateNumber;",
    function(err, results) {
    //console.log(results)
      response.send(results);
  });
});

app.get('/licensePlateNumber', function(request, response){
    connection.query("SELECT ttruckslicense.* FROM tdrivers\
                            RIGHT JOIN ttruckslicense\
                            ON tdrivers.truckId=ttruckslicense.truckId\
                            WHERE tdrivers.truckId is NULL;",
    function(err, results) {
      response.send(results);
  });
});


app.get('/unassignedOrders', function(request, response){
    connection.query("SELECT orderId AS id, orderName AS name, terminalId, destination , weightKg\
                        FROM vNonAssignedOrders;",
    function(err, results) {
      response.send(results);
  });
});


app.post('/terminalSelect', json, function(request, response){
    var query = '\
                SELECT *\
                FROM tterminals\
                WHERE tterminals.type=(?);';
            var parameters = [request.body.type];
            connection.execute(query, parameters)
            .then( result => {
                console.log(result[0])
                response.status(201).send(JSON.stringify(result[0]));
            })
})

app.get('/terminalSelectType', function(request, response){
    connection.query("SELECT  DISTINCT type FROM transporation.tterminals;",
    function(err, results) {
      response.send(results);
  });
});

app.post('/avtoTerminalSelect', json, function(request, response){
    var query = '\
                    SELECT * FROM transporation.vterminalstatistics\
                    WHERE vterminalstatistics.terminalType=(?)\
                    ORDER BY  vterminalstatistics.avgQueueTimeMin, vterminalstatistics.avgLoadingTimeMin DESC\
                    LIMIT 1 ;';
            var parameters = [request.body.type];
            connection.execute(query, parameters)
            .then( result => {
                console.log(result[0])
                response.status(201).send(JSON.stringify(result[0][0]));
            })
})



app.post('/assignedOrders', json, function(request, response){

    var query = '\
            SELECT *\
            FROM vAssignedOrders AS vao\
            INNER JOIN tOrderTransporation AS tot\
                ON tot.orderId = vao.orderId\
            INNER JOIN vDrivers AS vdr\
                ON tot.truckId = vdr.truckId\
            WHERE vdr.truckId= (?);';
            var parameters = [request.body.truckId];
            connection.execute(query, parameters)
            .then( result => {

                response.status(201).send(JSON.stringify(result[0]));
            })
})
app.get('/terminals', function(request, response){
    connection.query("SELECT * FROM transporation.vterminalstatistics ORDER BY terminalId;",
    function(err, results) {
      response.send(results);
  });
});

app.post('/planner', json, function(request, response){
    if(!request.body){
        response.sendStatus(STATUS_FAILURE);
    } else {
        console.log(request.body);
        dbWebPanel.driverPlanner(
            connection,
            request.body,
            function(status, message){ response.status(status).send(message); }
        );
    }
});

app.post('/cancelAssign', json, function(request, response){
    if(!request.body){
        response.sendStatus(STATUS_FAILURE);
    } else {
        console.log(request.body);
        dbWebPanel.cancelAssign(
            connection,
            request.body,
            function(status, message){ response.status(status).send(message); }
        );
    }
});
app.post('/deleteOrder', json, function(request, response){
    if(!request.body){
        response.sendStatus(STATUS_FAILURE);
    } else {
        console.log(request.body);
        dbWebPanel.deleteOrder(
            connection,
            request.body,
            function(status, message){ response.status(status).send(message); }
        );
    }
});
app.post('/adduser', json, function(request, response){
    if(!request.body){
        response.sendStatus(STATUS_FAILURE);
    } else {
        console.log(request.body);
        let { status, message } = dbWebPanel.insertNewDriver(connection, request.body);
        response.status(status).end(message);
    }
});
app.post('/addorder', json, function(request, response){
    if(!request.body){
        response.sendStatus(STATUS_FAILURE);
    } else {
        console.log(request.body);
        let { status, message } = dbWebPanel.insertNewOrder(connection, request.body);
        response.status(status).end(message);
    }
});
app.post('/assignOrder', json, function(request, response){
    if(!request.body){
        response.sendStatus(STATUS_FAILURE);
    } else {
        console.log(request.body);
        let { status, message } = dbWebPanel.assignOrder(connection, request.body);
        response.status(status).end(message);
    }
});
app.post('/auth', json, function(request, response){
    console.log(request.body);
    if (!request.body) {
        response.sendStatus(STATUS_FAILURE);
    } else {
        switch (request.body.request_type) {
            case 'authentication':
                dbAndroid.userAuth(connection, request.body, function(answer) { response.send(answer); });
                break;
            case 'get_order_list':
                dbAndroid.getOrderList(connection, request.body, function(answer) { response.send(answer); });
                break;
            case 'change_status':
                dbAndroid.updateOrderStatus(connection, request.body);
                break;
            default:
                response.status(STATUS_FAILURE).end('НЕПОНЯТНЫЙ REQUESET_TYPE');
        }
    }
});
app.listen(PORT, function(request, response){
    console.log('Server waiting for connection...');
});
