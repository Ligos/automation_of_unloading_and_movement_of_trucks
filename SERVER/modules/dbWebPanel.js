const STATUS_FAILURE = 400;
const STATUS_SUCCESS = 201;
const MESSAGE_SUCCESS = 'success';


module.exports.insertNewDriver = function (connection, userData){

    try {
        /* ============================================================
         * ВСТАВКА ПОЛЬЗОВАТЕЛЯ В tUsers (СОЗДАНИЕ ПОЛЬЗОВАТЕЛЯ)
         * ============================================================*/

        let status = STATUS_SUCCESS;
        let message = MESSAGE_SUCCESS;

        let login = createLoginByName(userData.firstName, userData.lastName, userData.fathersName);
        let email = login + '@kornienkocorp_dr.ru';
        let roleId = 2; // 2 - роль водителя в tRole

        let query = '\
        INSERT INTO tUsers(\
            roleId\
            , login\
            , firstName\
            , lastName\
            , fathersName\
            , birthday\
            , phoneNumber\
            , email\
        )\
        VALUES(?, ?, ?, ?, ?, ?, ?, ?);';

        let parameters = [
            roleId,
            login,
            userData.firstName,
            userData.lastName,
            userData.fathersName,
            userData.birthday,
            userData.phoneNumber,
            email
        ];

        connection.query(query, parameters)
            .then(result => console.log('ВСТАВКА ПОЛЬЗОВАТЕЛЯ' + result))
            .then(() => {
                /* ============================================================
                 * СОЗДАНИЕ УЧЁТНЫХ ДАННЫХ ПОЛЬЗОВАТЕЛЯ (СОЗДАНИЕ ПАРОЛЯ)
                 *          ДОБАВЛЕНИЕ ДАННЫХ В tCredentials
                 * ============================================================*/

                let driverId;
                let password = Math.random().toString(36).slice(-8); // пароль из 8 знаков
                query = '\
                    SELECT\
                        MAX(id) AS id\
                    FROM tUsers\
                    WHERE login = ?;';
                connection.execute(query, [login])
                    .then(result => {
                        console.log('ПОЛУЧЕНИЕ ID ВОДИТЕЛЯ' + result);
                        driverId = result[0][0].id
                    })
                    .then(() => {
                        query = '\
                            INSERT INTO tCredentials(\
                                userId\
                                ,password\
                            )\
                            VALUES(?, ?);';

                        connection.query(query, [driverId, password]).then(result => console.log('ВСТАВКА ПАРОЛЯ' + result));
                    })
                    .then( () => {
                        /* ============================================================
                         * ПРИВЯЗКА ВОДИТЕЛЯ К ГРУЗОВИКУ, ЕСЛИ ГРУЗОВИК УКАЗАН
                         *              ДОБАВЛЕНИЕ ДАННЫХ В tDrivers
                         * ============================================================*/
                        if (userData.truckLicense) {
                            let truckId;
                             query = '\
                                SELECT\
                                    truckId\
                                FROM tTrucksLicense\
                                WHERE licensePlateNumber = ?;';

                            return connection.execute(query, [userData.truckLicense.toUpperCase()])
                                .then(result => {
                                    truckId = result[0][0].truckId;
                                    console.log('ПОЛУЧЕНИЕ ID МАШИНЫ' + result);
                                })
                                .then(() => {
                                    let status = 1; // статус водителя "свободен"
                                    query = '\
                                        INSERT INTO tDrivers(\
                                            userId\
                                            , truckId\
                                            , statusId\
                                        )\
                                        VALUES(?, ?, ?);';

                                    connection.query(query, [driverId, truckId, status]).then(result => console.log('ВСТАВКА ВОДИТЕЛЯ' + result));
                                })
                                .then(result => console.log(result))
                                .catch(error => {
                                    status = STATUS_FAILURE;
                                    message = error;
                                });
                        }
                    })
                    .then(result => console.log(result))
                    .catch(error => {
                        status = STATUS_FAILURE;
                        message = error;
                    });
            })
            .catch(error => {
                status = STATUS_FAILURE;
                message = error;
                console.log(error);
            });



        return { status, message };

    }
    catch(error) {
        let status = STATUS_FAILURE;
        let message = error;
        return { status, message };
    }
};

module.exports.insertNewOrder = function (connection, userData){
    try {

        let status = STATUS_SUCCESS;
        let message = MESSAGE_SUCCESS;

        let terminalId = -1;

        let query = '\
            SELECT\
                id\
            FROM tTerminals\
            WHERE LOWER(name) = LOWER(?);';
        let parameters = [userData.terminalName];

        connection.execute(query, parameters)
            .then(result => {
                console.log('ПОЛУЧЕНИЕ ID ТЕРМИНАЛА: ');
                console.log(result);
                terminalId = result[0][0].id;
            })
            .then(() => {
                if (terminalId != -1 || terminalId != undefined){
                    const millimeters = 1000;
                    query = '\
                        INSERT INTO tOrders(\
                            name\
                            , terminalId\
                            , weightKg\
                            , lengthMet\
                            , widthMet\
                            , heightMet\
                            , cityDestination\
                            , addressDestination\
                        )\
                        VALUES (?, ?, ?, ?, ?, ?, ?, ?);';
                    parameters = [
                        userData.orderName,
                        terminalId,
                        userData.weightKg,
                        userData.lengthMet / millimeters,
                        userData.widthMet / millimeters,
                        userData.heightMet / millimeters,
                        userData.cityDestination,
                        userData.addressDestination
                    ];

                    connection.query(query, parameters)
                        .then(result => {
                            console.log('РЕЗУЛЬТАТ ВСТАВКИ ЗАКАЗА: ');
                            console.log(result);
                        })
                        .catch(error => {
                            status = STATUS_FAILURE;
                            message = error;
                            console.log('ОШИБКА: ' + message);
                        });
                } else {

                    let errorMsg = 'ТАКОГО ТЕРМИНАЛА НЕТ В СПРАВОЧНИКЕ';
                    console.log(errorMsg);
                    status = STATUS_FAILURE;
                    message = errorMsg;
                }
            })
            .catch(error => {
                status = STATUS_FAILURE;
                message = error;
                console.log('ОШИБКА: ' + message);
            });

        return { status, message };

    } catch (e) {
        let status = 400;
        let message = e.message();

        return { status, message };
    }
};

module.exports.assignOrder = function (connection, userData){

    try {
        let status = STATUS_SUCCESS;
        let message = MESSAGE_SUCCESS;

        let query = 'CALL prcAssignOrder(?, ?)';
        let parameters = [userData.truckLicense, userData.orderId];

        connection.execute(query, parameters)
            .then(() => {
                console.log('ЗАКАЗ НАЗНАЧЕН УСПЕШНО');
            })
            .catch(error => {
                console.log('ЗАКАЗ НЕ НАЗНАЧЕН. ОШИБКА:\n', error);
                let status = STATUS_FAILURE;
                let message = error;
            })

        return { status, message };

    } catch (error) {

        let status = STATUS_FAILURE;
        let mesage = error.message();

        return { status, message };
    }
};

module.exports.cancelAssign = function (connection, userData, callback){
    try {
        let status = STATUS_SUCCESS;
        let message = MESSAGE_SUCCESS;

        let query = 'CALL prcDeleteTransporation(?)';
        let parameters = [userData.orderId];

        connection.query(query, parameters)
            .then(() => {
                console.log('УДАЛЕНИЕ ЗАКАЗА У ВОДИТЕЛЯ ПРОШЛО УСПЕШНО');
                callback(status, message);
            })
            .catch(error => {
                console.log('УДАЛЕНИЕ ЗАКАЗА У ВОДИТЕЛЯ ЗАВЕРШИЛОСЬ С ОШИБКОЙ:');
                status = STATUS_FAILURE;
                message = error;
                console.log(message);
                callback(status, message);
            });

    } catch (e) {
        let status = STATUS_FAILURE;
        let message = e.message();
        callback(status, message);
    }
};

module.exports.driverPlanner = function (connection, userData, callback) {
    try {
        let status = STATUS_SUCCESS;
        let message = MESSAGE_SUCCESS;

        let query = 'CALL prcDriversPlanning(?)';
        let parameters = [userData.driverId];

        connection.query(query, parameters)
            .then(() => {
                console.log('ПЛАНИРОВЩИК ОТРАБОТАЛ УСПЕШНО');
                callback(status, message);
            })
            .catch(error => {
                console.log('ПЛНИРОВЩИК ОТРАБОТАЛ С ОШИБКАМИ:');
                status = STATUS_FAILURE;
                message = {...error}.message;
                console.log(message);
                callback(status, message);
            });

    } catch (e) {
        let status = STATUS_FAILURE;
        let message = e.message();

        callback(status, message);
    }
};

module.exports.deleteOrder = function (connection, userData, callback) {
    try {
        let status = STATUS_SUCCESS;
        let message = MESSAGE_SUCCESS;

        let query = 'CALL prcDeleteOrder(?);';
        let parameters = [userData.orderId];

        connection.query(query, parameters)
            .then(() => {
                console.log('ЗАКАЗ УДАЛЁН УСПЕШНО');
                callback(status, message);
            })
            .catch(error => {
                console.log('ОШИБКА. ЗАКАЗ НЕ УДАЛЁН:');
                status = STATUS_FAILURE;
                message = error;
                console.log(message);
                callback(status, message);
            });

    } catch (e) {
        let status = STATUS_FAILURE;
        let message = e.message();
        callback(status, message);
    } finally {

    }
};

function createLoginByName(firstName, lastName, fathersName){
    let ru = {
        'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd',
        'е': 'e', 'ё': 'e', 'ж': 'j', 'з': 'z', 'и': 'i', 'й':'i',
        'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n', 'о': 'o',
        'п': 'p', 'р': 'r', 'с': 's', 'т': 't', 'у': 'u',
        'ф': 'f', 'х': 'h', 'ц': 'c', 'ч': 'ch', 'ш': 'sh',
        'щ': 'shch', 'ы': 'y', 'э': 'e', 'ю': 'u', 'я': 'ya'
    };
    let login = [];

    firstName = firstName.toLowerCase().replace(/[ъь]+/g, '');
    lastName = lastName.toLowerCase().replace(/[ъь]+/g, '');

    if (fathersName){
        fathersName = fathersName.replace(/[ъь]+/g, '');
    };

    for (let i = 0; i < lastName.length; ++i ) {
       login.push(
              ru[ lastName[i] ]
           || ru[ lastName[i].toLowerCase() ] == undefined && lastName[i]
           || ru[ lastName[i].toLowerCase() ].replace(/^(.)/, function (match) { return match.toUpperCase(); })
       );
   };

    login = login.join('') + '_' + ru[firstName[0]] + (fathersName ? ru[fathersName[0]] : '');

    return login;
};

// всё что ниже - бездумный копи-паст из stack overflow
function twoDigits(d) {
    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
}
Date.prototype.toMysqlFormat = function() {
    return this.getFullYear() + "-"
            + twoDigits(1 + this.getMonth()) + "-"
            + twoDigits(this.getDate()) + " "
            + twoDigits(this.getHours()) + ":"
            + twoDigits(this.getMinutes()) + ":"
            + twoDigits(this.getSeconds());
};
