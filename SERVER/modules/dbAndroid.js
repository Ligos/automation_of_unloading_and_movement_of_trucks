module.exports.userAuth = function (connection, userData, callback){
    try {
        let credentials;
        let answer;
        let driverId = -1;
        let truckId = -1;

        Authentication(connection, userData.login, userData.password, function(ans) {
            answer = ans;
            getDriverId(connection, userData.login, function(drId) {
                driverId = drId;
                getOrderList(connection, driverId, function(orderList){
                    answer.data = orderList;
                    answer.id = driverId;

                    return callback(answer);
                });
            });
        });

    } catch (e) {
        let status = STATUS_SUCCESS;
        let message = MESSAGE_SUCCESS;
        let answer = {
            status: 'FAILED',
            keyword: '-'
        };
        return callback(answer);
    }
};

module.exports.getOrderList = function (connection, userData, callback) {
    try {

        getOrderList(connection, userData.id, function(orderList) {
            let answer = {
                status: 'OK',
                data: orderList
            }
            return callback(answer);
        });

    } catch (e) {
        let status = STATUS_SUCCESS;
        let message = MESSAGE_SUCCESS;
        let answer = {
            status: 'FAILED',
            data: {}
        };
        return callback(answer);
    }
};

module.exports.updateOrderStatus = function (connection, userData){
    try{
        updateOrderStatus(connection, userData.order_id, userData.new_status);
    } catch (error) {
        console.log('ОШИБКА В updateOrderStatus:\n', error);
    }

};


function Authentication(connection, login, password, callback) {
    let query = '\
        SELECT\
            1 AS isAuth\
        FROM vCredentials\
        WHERE LOWER(login) = LOWER(?)\
            AND password = ?;';
    let parameters = [login, password];
    let answer = {
        status: 'FAILED',
        keyword: '-'
    };
    connection.execute(query, parameters)
        .then(result => {
            if (result[0][0].isAuth == 1){
                let keyword = Math.random().toString(36).slice(-8); // пароль из 8 знаков;
                answer.status = 'OK';
                answer.keyword = keyword;
                return callback(answer);
            } else {
                return callback(answer);
            }
        })
        .catch(error => {
            console.log('ОШИБКА В АВТОРИЗАЦИИ:');
            console.log(error);
            return callback(answer);
        });
}

function getDriverId(connection, login, callback) {
    let driverId = -1;

    let query = '\
        SELECT\
            id\
        FROM tUsers\
        WHERE LOWER(login) = LOWER(?);';
    let parameters = [login];

    connection.execute(query, parameters)
        .then(result => {
            driverId = result[0][0].id;
            return callback(driverId);
        })
        .catch(error => {
            console.log('ОШИБКА В ПОЛУЧЕНИИ ID ВОДИТЕЛЯ:');
            console.log(error);
            return callback(driverId);
        });
};

function getOrderList(connection, driverId, callback) {
    let orderList;

    let query = '\
        SELECT\
            vao.orderId\
            , vao.orderName\
            , vao.terminalName\
            , vao.weightKg\
            , vao.lengthMet\
            , vao.widthMet\
            , vao.heightMet\
            , vao.statusId AS status\
            , vao.destination\
        FROM vDrivers AS vdr\
        INNER JOIN tOrderTransporation AS tot\
            ON tot.truckId = vdr.truckId\
        INNER JOIN vAssignedOrders AS vao\
            ON vao.orderId = tot.orderId\
        WHERE vdr.driverId = ?;';
        let parameters = [driverId];

        connection.execute(query, parameters)
            .then(result => {
                orderList = result[0];
                console.log(orderList);
                return callback(orderList);
            })
            .catch(error => {
                console.log('ОШИБКА В ПОЛУЧЕНИИ СПИСКА ЗАКАЗОВ:');
                console.log(error);
                return callback(orderList);
            });
};

function updateOrderStatus(connection, orderId, statusId) {
    let query = 'CALL prcUpdateOrderStatus (?, ?);';
    let parameters = [orderId, statusId];

    connection.query(query, parameters)
        .then(() => {
            console.log('ВСТАВКА НОВОГО СТАТУСА ПРОШЛА УСПЕШНО');
        })
        .catch(error => {
            console.log('ОШИБКА ВСТАВКИ НОВОГО СТАТУСА:\n', error);
        });
};
