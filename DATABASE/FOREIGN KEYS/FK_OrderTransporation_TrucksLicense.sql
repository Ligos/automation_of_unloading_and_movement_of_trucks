USE TRANSPORATION;

ALTER TABLE tOrderTransporation
    ADD CONSTRAINT FK_OrderTransporation_TrucksLicense FOREIGN KEY (truckId)
    REFERENCES tTrucksLicense (truckId)
    ON DELETE RESTRICT
    ON UPDATE CASCADE;


