CREATE DATABASE IF NOT EXISTS TRANSPORATION;


USE TRANSPORATION;

-- таблица с данными для аутентификации пользовалей
CREATE TABLE IF NOT EXISTS tCredentials(
    userId      INTEGER     NOT NULL -- id пользователя
    , password  LONGTEXT    NOT NULL -- пароль пользователя
    , CONSTRAINT PK_Credentials PRIMARY KEY (userId)
);


USE TRANSPORATION;

-- таблица с привязкой человека и грузовика
CREATE TABLE IF NOT EXISTS tDrivers(
    userId      INTEGER     NOT NULL -- id пользователя
    , truckId   INTEGER     NOT NULL -- id грузовика
    , statusId  INTEGER     NOT NULL -- текущий статус водителя и грузовика
    , startDtm  DATETIME    NOT NULL DEFAULT CURRENT_TIMESTAMP -- дата и время начала действия статуса
    , CONSTRAINT PK_Drivers PRIMARY KEY (userId, truckId)
);


USE TRANSPORATION;

DROP TABLE IF EXISTS tOrders;

-- таблица с заказами
CREATE TABLE IF NOT EXISTS tOrders(
    id                      INTEGER     NOT NULL AUTO_INCREMENT -- id заказа
    , name                  VARCHAR(512)NOT NULL -- наименование заказа
    , terminalId            INTEGER     NOT NULL -- id терминала, на котором располагается заказ
    , weightKg              FLOAT       NOT NULL -- вес заказа в кг
    , lengthMet             FLOAT       NOT NULL -- длина заказа в метрах
    , widthMet              FLOAT       NOT NULL -- ширина заказа в метрах
    , heightMet             FLOAT       NOT NULL -- высота заказа в метрах
    , cityDestination       VARCHAR(512)    NULL -- город назначения
    , addressDestination    VARCHAR(512)    NULL -- адрес назначения
    , createDtm             DATETIME    NOT NULL DEFAULT CURRENT_TIMESTAMP -- дата и время создания заказа
    , CONSTRAINT PK_Orders PRIMARY KEY (id)
);


USE TRANSPORATION;

-- таблица с текущим состоянием заказа
CREATE TABLE IF NOT EXISTS tOrderStatusCurrent(
    orderId             INTEGER     NOT NULL -- id заказа
    , statusId          INTEGER     NOT NULL -- id статуса заказа
    , startDtm          DATETIME    NOT NULL DEFAULT CURRENT_TIMESTAMP -- дата и время начала действия статуса
    , CONSTRAINT PK_OrderStatusCurrent PRIMARY KEY (orderId)
);


USE TRANSPORATION;

-- таблица с историей состояний заказа
CREATE TABLE IF NOT EXISTS tOrderStatusHistory(
    orderId                 INTEGER     NOT NULL -- id заказа
    , orderName             VARCHAR(512)NOT NULL -- наименование заказа
    , terminalId            INTEGER     NOT NULL -- id терминала
    , truckId               INTEGER     NOT NULL -- id грузовика
    , createDtm             DATETIME    NOT NULL -- дата и время создания заявки
    , statusId              INTEGER     NOT NULL -- статус заказа
    , startStatusDtm        DATETIME    NOT NULL -- дата и время назначения статуса заказа водителю/грузовику
    , completeStatusDtm     DATETIME    NOT NULL DEFAULT CURRENT_TIMESTAMP -- дата и время окончания статуса заказа
);


USE TRANSPORATION;

-- таблица с заказом и привязанным к нему грузовиком
CREATE TABLE IF NOT EXISTS tOrderTransporation(
    orderId     INTEGER NOT NULL -- id заказа
    , truckId   INTEGER NOT NULL -- id грузовика
    , CONSTRAINT PK_OrderTransporation PRIMARY KEY (orderId)
);


USE TRANSPORATION;

-- таблица-справочник ролей пользователей
CREATE TABLE IF NOT EXISTS tRole(
    id      INTEGER         NOT NULL AUTO_INCREMENT -- id роли
    , name  VARCHAR(128)    NOT NULL -- наименование роли
    , CONSTRAINT PK_Role PRIMARY KEY (id)
);


USE TRANSPORATION;

-- таблица-справочник статусов
CREATE TABLE IF NOT EXISTS tStatus(
    id          INTEGER         NOT NULL AUTO_INCREMENT -- id статуса
    , typeId    SMALLINT        NOT NULL -- id тип статуса
    , name      VARCHAR(128)    NOT NULL -- наименование статуса
    , CONSTRAINT PK_Status PRIMARY KEY (id)
);


USE TRANSPORATION;

-- таблица-справочник с типами статуса
CREATE TABLE IF NOT EXISTS tStatusType(
    id      SMALLINT        NOT NULL AUTO_INCREMENT -- id типа статуса
    , name  VARCHAR(128)    NOT NULL -- кому относится статус
    , CONSTRAINT PK_StatusType PRIMARY KEY (id)
);


USE TRANSPORATION;

-- таблица-справочник по терминалам
CREATE TABLE IF NOT EXISTS tTerminals(
    id      INTEGER         NOT NULL AUTO_INCREMENT -- id терминала
    , type  VARCHAR(128) 	NOT NULL -- тип терминала
    , name  VARCHAR(128)    NOT NULL -- наименование терминала
    , CONSTRAINT PK_Terminals PRIMARY KEY (id)
);


USE TRANSPORATION;

-- таблица со статусами терминала (текущие и прошедшие)
CREATE TABLE IF NOT EXISTS tTerminalStatus(
    terminalId  INTEGER     NOT NULL -- id терминала
    , statusId  INTEGER     NOT NULL -- id статуса
    , startDtm  DATETIME    NOT NULL DEFAULT CURRENT_TIMESTAMP -- начала действия статуса
    , CONSTRAINT PK_TerminalStatus PRIMARY KEY (terminalId, startDtm)
);


USE TRANSPORATION;

-- таблица-справочник с грузовиками, имеющиеся у компании
CREATE TABLE IF NOT EXISTS tTrucksLicense(
    truckId                 INTEGER     NOT NULL AUTO_INCREMENT -- id грузовика
    , modelId               INTEGER     NOT NULL -- id модели грузовика
    , licensePlateNumber    VARCHAR(16) NOT NULL -- номерные знаки грузовика
    , CONSTRAINT PK_TrucksLicense PRIMARY KEY (truckId)
);


USE TRANSPORATION;

-- таблица-справочник с моделями и показателями грузовиков
CREATE TABLE IF NOT EXISTS tTrucksModel(
    id              INTEGER         NOT NULL AUTO_INCREMENT -- id типа грузовика
    , manufacturer  VARCHAR(128)    NOT NULL -- наименование производителя
    , model         VARCHAR(128)    NOT NULL -- наименование модели
    , lengthMet     FLOAT           NOT NULL -- длина грузовка в метрах
    , widthMet      FLOAT           NOT NULL -- ширина грузовика в метрах
    , heightMet     FLOAT           NOT NULL -- высотка грузовки в метрах
    , carWeightKg   FLOAT               NULL -- вес автомобиля в килограммах
    , maxVolumeMet  FLOAT           NOT NULL -- максимальный объём груза в метрах кубических, что может взять грузовик
    , payLoadKg     FLOAT           NOT NULL -- вес полезного груза в кг
    , CONSTRAINT PK_TrucksModel PRIMARY KEY (id)
);


USE TRANSPORATION;

-- таблица-справочник пользователей
CREATE TABLE IF NOT EXISTS tUsers(
    id              INTEGER         NOT NULL AUTO_INCREMENT -- id пользователя
    , roleId        INTEGER         NOT NULL -- id роли
    , login         VARCHAR(128)    NOT NULL -- логин
    , firstName     VARCHAR(128)    NOT NULL -- имя
    , lastName      VARCHAR(128)    NOT NULL -- фамилия
    , fathersName   VARCHAR(128)        NULL -- отчество
    , birthday      DATE                NULL -- дата рождения
    , phoneNumber   VARCHAR(14)         NULL -- номер телефона
    , email         VARCHAR(128)    NOT NULL -- корпоративная почта
    , CONSTRAINT PK_Users PRIMARY KEY (id)
);


USE TRANSPORATION;

CREATE OR REPLACE VIEW vAssignedOrders
AS
SELECT
    osc.orderId
    , ord.name AS orderName
    , osc.startDtm
    , osc.statusId
    , ts.name AS statusName
    , ord.terminalId
    , term.name AS terminalName
    , ord.weightKg
    , ord.lengthMet
    , ord.widthMet
    , ord.heightMet
    , CONCAT_WS(' ', 'г.', ord.cityDestination, ord.addressDestination) AS destination
    , ord.createDtm
FROM tOrderStatusCurrent AS osc 
INNER JOIN tOrders AS ord
    ON osc.orderId = ord.id
INNER JOIN tTerminals AS term
    ON ord.terminalId = term.id
INNER JOIN tStatus AS ts
    ON ts.id = osc.statusId
WHERE osc.statusId BETWEEN 4 AND 7;



USE TRANSPORATION;

CREATE OR REPLACE VIEW vCredentials
AS
SELECT
    us.login
    , credit.password
FROM tCredentials AS credit
INNER JOIN tUsers AS us
    ON credit.userId = us.id;


USE TRANSPORATION;

CREATE OR REPLACE VIEW vNonAssignedOrders
AS
SELECT
    ord.id AS orderId
    , ord.name AS orderName
    , ord.terminalId
    , term.name AS terminalName
    , ord.weightKg
    , ord.lengthMet
    , ord.widthMet
    , ord.heightMet
    , CONCAT_WS(' ', 'г.', ord.cityDestination, ord.addressDestination) AS destination
    , ord.createDtm
FROM tOrders AS ord
LEFT JOIN tOrderStatusCurrent AS osc
    ON osc.orderId = ord.id
INNER JOIN tTerminals AS term
    ON ord.terminalId = term.id
WHERE osc.orderId IS NULL
    OR osc.statusId = 3; -- статус "не назначен" для заказа


USE TRANSPORATION;

CREATE OR REPLACE VIEW vStatus
AS
SELECT
    tst.id AS typeId
    , tst.name AS typeName
    , ts.id AS statusId
    , ts.name AS statusName
FROM tStatus AS ts
INNER JOIN tStatusType AS tst
    ON tst.id = ts.typeId;

USE TRANSPORATION;

CREATE OR REPLACE VIEW vTrucks
AS
SELECT
    tl.truckId
    , tm.manufacturer
    , tm.model
    , tm.lengthMet
    , tm.widthMet
    , tm.heightMet
    , tm.carWeightKg
    , tm.maxVolumeMet
    , tm.payLoadKg
    , tl.licensePlateNumber
FROM tTrucksLicense AS tl
INNER JOIN tTrucksModel AS tm
    ON tl.modelId = tm.id;


USE TRANSPORATION;

CREATE OR REPLACE VIEW vTerminalStatistics
AS
SELECT
    term.id AS terminalId
    , term.type AS terminalType
    , term.name AS terminalName
    , COUNT(DISTINCT osh.orderId) AS ordersHistoryCnt -- кол-во обработанных заказов
    , COUNT(DISTINCT osc.orderId) AS orderCurrentCnt -- кол-во текущий заявок в очереди
    , ROUND(AVG(oshQueue.queueTimeSec) / 60, 2) AS avgQueueTimeMin -- среднее время заказа в очереди в минутах
    , ROUND(AVG(oshLoading.loadingTimeSec) / 60, 2)  AS avgLoadingTimeMin -- среднее время погрузки в минутах
FROM tTerminals AS term
LEFT JOIN tOrderStatusHistory AS osh
    ON term.id = osh.terminalId
LEFT JOIN (
    SELECT
        orderId
        , terminalId
        , TIMESTAMPDIFF(SECOND, startStatusDtm, completeStatusDtm) AS queueTimeSec
    FROM tOrderStatusHistory
    WHERE statusId = 5
) AS oshQueue
    ON term.id = oshQueue.terminalId
    AND osh.orderId = oshQueue.orderId
    AND osh.statusId = 5
LEFT JOIN (
    SELECT
        orderId
        , terminalId
        , TIMESTAMPDIFF(SECOND, startStatusDtm, completeStatusDtm) AS loadingTimeSec
    FROM tOrderStatusHistory
    WHERE statusId = 6
) AS oshLoading
    ON term.id = oshLoading.terminalId
    AND osh.orderId = oshLoading.orderId
    AND osh.statusId = 6
LEFT JOIN tOrders AS tord
    ON term.id = tord.terminalId
LEFT JOIN tOrderStatusCurrent AS osc
    ON tord.id = osc.orderId
    AND osc.statusId IN (3, 4, 5, 6) -- не назначен, назначен, осмотр, погрузка
GROUP BY
    osh.terminalId
    , term.name;


USE TRANSPORATION;

CREATE OR REPLACE VIEW vDrivers
AS
SELECT
    dr.userId AS driverId
    , us.login
    , us.firstName
    , us.lastName
    , us.fathersName
    , us.phoneNumber
    , dr.statusId
    , dr.truckId
    , tr.licensePlateNumber
FROM(
    SELECT
        userId
        , truckId
        , statusId
        , MAX(startDtm) AS startDtm
    FROM tDrivers
    GROUP BY
        userId
        , truckId
        , statusId
) AS dr
INNER JOIN tUsers AS us
    ON dr.userId = us.id
INNER JOIN vTrucks AS tr
    ON tr.truckId = dr.truckId;

USE TRANSPORATION;

CREATE OR REPLACE VIEW vWeightOrderRank
AS
SELECT
    ord.id AS orderId
    , ord.name AS orderName
    , term.id AS terminalId
    , term.type AS terminalType 
    , vdr.driverId
    , ord.weightKg
    , ord.cityDestination
    , ROW_NUMBER() OVER (
        PARTITION BY
            ord.cityDestination
            , term.type
        ORDER BY
            ord.weightKg DESC
            , vts.avgQueueTimeMin ASC
            , vts.avgLoadingTimeMin ASC
    ) AS orderRank
FROM tOrders AS ord
INNER JOIN tTerminals AS term
    ON ord.terminalId = term.id
LEFT JOIN vTerminalStatistics AS vts
    ON vts.terminalId = ord.terminalId
LEFT JOIN tOrderTransporation AS tot
    ON tot.orderId = ord.id
LEFT JOIN vDrivers AS vdr
    ON vdr.truckId = tot.truckId;





USE TRANSPORATION;

ALTER TABLE tCredentials
    ADD CONSTRAINT FK_Credentials_Users FOREIGN KEY (userId)
    REFERENCES tUsers (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE;


USE TRANSPORATION;

ALTER TABLE tDrivers
    ADD CONSTRAINT FK_Drivers_Status FOREIGN KEY (statusId)
    REFERENCES tStatus (id)
    ON DELETE RESTRICT
    ON UPDATE CASCADE;
USE TRANSPORATION;

ALTER TABLE tDrivers
    ADD CONSTRAINT FK_Drivers_TrucksLicense FOREIGN KEY (truckId)
    REFERENCES tTrucksLicense (truckId)
    ON DELETE RESTRICT
    ON UPDATE CASCADE;


USE TRANSPORATION;

ALTER TABLE tDrivers
    ADD CONSTRAINT FK_Drivers_Users FOREIGN KEY (userId)
    REFERENCES tUsers (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE;


USE TRANSPORATION;

ALTER TABLE tOrderStatusCurrent
    ADD CONSTRAINT FK_OrderStatusCurrent_Orders FOREIGN KEY (orderId)
    REFERENCES tOrders (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE;


USE TRANSPORATION;

ALTER TABLE tOrderStatusCurrent
    ADD CONSTRAINT FK_OrderStatusCurrent_Status FOREIGN KEY (statusId)
    REFERENCES tStatus (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE;


USE TRANSPORATION;

ALTER TABLE tOrders
    ADD CONSTRAINT FK_Orders_Terminals FOREIGN KEY (terminalId)
    REFERENCES tTerminals (id)
    ON DELETE RESTRICT
    ON UPDATE CASCADE;


USE TRANSPORATION;

ALTER TABLE tOrderTransporation
    ADD CONSTRAINT FK_OrderTransporation_Orders FOREIGN KEY (orderId)
    REFERENCES tOrders (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE;


USE TRANSPORATION;

ALTER TABLE tOrderTransporation
    ADD CONSTRAINT FK_OrderTransporation_TrucksLicense FOREIGN KEY (truckId)
    REFERENCES tTrucksLicense (truckId)
    ON DELETE RESTRICT
    ON UPDATE CASCADE;


USE TRANSPORATION;

ALTER TABLE tStatus
    ADD CONSTRAINT FK_Status_StatusType FOREIGN KEY (typeId)
    REFERENCES tStatusType (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE;


USE TRANSPORATION;

ALTER TABLE tTerminalStatus
    ADD CONSTRAINT FK_TerminalStatus_Status FOREIGN KEY (statusId)
    REFERENCES tStatus (id)
    ON DELETE RESTRICT
    ON UPDATE CASCADE;


USE TRANSPORATION;

ALTER TABLE tTerminalStatus
    ADD CONSTRAINT FK_TerminalStatus_Terminals FOREIGN KEY (terminalId)
    REFERENCES tTerminals (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE;


USE TRANSPORATION;

ALTER TABLE tTrucksLicense
    ADD CONSTRAINT FK_TrucksLicense_TrucksModel FOREIGN KEY (modelId)
    REFERENCES tTrucksModel (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE;


USE TRANSPORATION;

ALTER TABLE tUsers
    ADD CONSTRAINT FK_Users_Role FOREIGN KEY (roleId)
    REFERENCES tRole (id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT;


USE TRANSPORATION;

INSERT INTO tRole(
    name
)
VALUES('Администратор панели')
    ,('Водитель')
    ,('Аналитик');


USE TRANSPORATION;

INSERT INTO tUsers(
    roleId
    , login
    , firstName
    , lastName
    , fathersName
    , birthday
    , email
)
VALUES(1,   'kornienko_ds', 'Корниенко','Денис',    'Сергеевич',    CAST('19980708' AS DATE),   'kornienko_ds@kornienkocorp.com')
    ,(2,    'masenko_ie',   'Масенко',  'Игорь',    'Евгеньевич',   CAST('19971205' AS DATE),   'masenko@kornienkocorp_dr.com')
    ,(2,    'drobot_va',    'Дробот',   'Василий',  'Александрович',CAST('19980823' AS DATE),   'drobot_va@kornienkocorp_dr.com')
    ,(3,    'ivanov_ii',    'Иванов',   'Иван',     'Ильич',        CAST('19900808' AS DATE),   'ivanov_ii@kornienkocorp.com')
    ,(2,    'spidorod_gp',  'Спидоров', 'Геннадий', 'Павлович',     CAST('19780921' AS DATE),   'spidorod_gp@kornienkocorp_dr.com');


USE TRANSPORATION;

INSERT INTO tTerminals(
    type
    , name
)
VALUES('Алкоголь','Алкоголь-1'), ('Алкоголь','Алкоголь-2'), ('Молочная_продукция','МП_1'), ('Молочная_продукция','МП_2'), ('Растительного_происхождения','РП_1'), ('Растительного_происхождения','РП_2'), 
    ('Растительного_происхождения','РП_3'),('Растительного_происхождения','РП_4'), ('Быто-химимческий', 'БХ-1'), ('Быто-химимческий', 'БХ-2'), ('Быто-химимческий', 'БХ-3'), ('Мясной', 'М-1'), ('Мясной', 'М-2')
    , ('Мясной', 'М-3'), ('Мясной', 'М-4');

USE TRANSPORATION;

INSERT INTO tStatusType(
    name
)
VALUES('Заказ'), ('Водитель'), ('Терминал');


USE TRANSPORATION;

INSERT INTO tStatus(
    typeId
    , name
)
VALUES(2, 'Свободен')
    ,(2, 'В работе')
    ,(1, 'Не назначен')
    ,(1, 'Назначен')
    ,(1, 'Осмотр')
    ,(1, 'Погрузка')
    ,(1, 'Погрузка окончена')
    ,(1, 'Доставлено')
    ,(3, 'Свободен')
    ,(3, 'Занят');

USE TRANSPORATION;

INSERT INTO tCredentials(
    userId
    , password
)
VALUES(1, 'GfvA4s6K1'), (2, '12345678'), (3, '12345678')
    ,(4, '12345678'), (5, '12345678');



USE TRANSPORATION;

-- цифры вбивались на обум, так что возможно их перебивать
INSERT INTO tTrucksModel(
    manufacturer
    , model
    , lengthMet
    , widthMet
    , heightMet
    , carWeightKg
    , maxVolumeMet
    , payLoadKg
)
VALUES('MAN', 'TGL', 5.870, 2.240, 2.531, 2000, 4.3, 8000);


USE TRANSPORATION;

INSERT INTO tTrucksLicense(
    modelId
    , licensePlateNumber
)
VALUES(1, 'A123AA123RUS')
    ,(1, 'E111EE777RUS')
    ,(1, 'B777OP66RUS')
    ,(1, 'X666AM55RUS')
    ,(1, 'A333BC01RUS')
    ,(1, 'T342RA61RUS')
    ,(1, 'H528OP123RUS');


USE TRANSPORATION;

INSERT INTO tDrivers(
    userId
    , truckId
    , statusId
)
VALUES(2, 2, 1)
,(3, 3, 1)
,(5, 1, 1);


USE TRANSPORATION;

INSERT INTO tOrders(
    name
    , terminalId
    , weightKg
    , lengthMet
    , widthMet
    , heightMet
    , cityDestination
    , addressDestination
)
VALUES('Кубанская бурёнка', 1, 34.5, 0.5, 0.5, 0.5, 'Краснодар', 'ул. Совхозная, 123');


USE TRANSPORATION;

DROP PROCEDURE IF EXISTS prcAssignOrder;

DELIMITER // 

CREATE PROCEDURE prcAssignOrder(
    IN param_truckLicense VARCHAR(16)
    , IN param_orderId INTEGER
)
BEGIN

    DECLARE truckId INTEGER;
    DECLARE orderStatusId INTEGER DEFAULT 4; -- 4 статус "назначен" для заказа
    DECLARE driverStatusId INTEGER DEFAULT 2; -- 2 статус "в работе" для водителя
    DECLARE driverId INTEGER;
    DECLARE dclCurrentStatus INTEGER DEFAULT -1;
    
    SET truckId = (SELECT ttl.truckId FROM tTrucksLicense AS ttl WHERE LOWER(ttl.licensePlateNumber) = LOWER(param_truckLicense));
    SET driverId = (SELECT tdr.userId FROM tDrivers AS tdr WHERE tdr.truckId = truckId);
    SET dclCurrentStatus = (SELECT statusId FROM tOrderStatusCurrent WHERE orderId = param_orderId);
    
    INSERT INTO tOrderTransporation(
        orderId
        , truckId
    )
    VALUES (param_orderId, truckId);
    
    IF (dclCurrentStatus = -1 OR dclCurrentStatus IS NULL) THEN

        INSERT INTO tOrderStatusCurrent(
            orderId
            , statusId
        )
        VALUES(param_orderId, orderStatusId);

    ELSEIF (dclCurrentStatus = 3) THEN

        UPDATE tOrderStatusCurrent
        SET statusId = 4
            ,startDtm = CURRENT_TIMESTAMP()
        WHERE orderId = param_orderId;

    END IF;
        
    UPDATE tDrivers
    SET statusId = driverStatusId
        , startDtm = CURRENT_TIMESTAMP()
    WHERE userId = driverId
        AND statusId <> 2;

END //


USE TRANSPORATION;

DROP PROCEDURE IF EXISTS prcDeleteTransporation;

DELIMITER //

CREATE PROCEDURE prcDeleteTransporation( IN param_orderId INTEGER)
BEGIN

    DECLARE dclStatusId INTEGER DEFAULT 3; -- 3 - статус "не назначен" для заказа
    DECLARE dclDriverId INTEGER DEFAULT -1;
    DECLARE dclIsDriverFree TINYINT DEFAULT 0;
    
    SET dclDriverId = (
        SELECT
            vdr.driverId
        FROM tOrderTransporation AS tot
        INNER JOIN vDrivers AS vdr
            ON tot.truckId = vdr.truckId
        WHERE tot.orderId = param_orderId
    );
    
    DELETE tot
    FROM tOrderTransporation AS tot
    WHERE tot.orderId = param_orderId;

    UPDATE tOrderStatusCurrent AS osc
    SET osc.statusId = dclStatusId
        , osc.startDtm = CURRENT_TIMESTAMP()
    WHERE osc.orderId = param_orderId;
    
    SET dclIsDriverFree = (
        SELECT
            CASE
                WHEN COUNT(tot.orderId) IS NULL OR COUNT(tot.orderId) = 0
                THEN 1
                ELSE 0
            END
        FROM tOrderTransporation AS tot
        INNER JOIN vDrivers AS vdr
            ON vdr.truckId = tot.truckId
        WHERE vdr.driverId = dclDriverId
    );
    
    IF dclIsDriverFree = 1 THEN
        UPDATE tDrivers
        SET statusId = 1
            , startDtm = CURRENT_TIMESTAMP()
        WHERE userId = dclDriverId;
    END IF;
END //


USE TRANSPORATION;

DROP PROCEDURE IF EXISTS prcDriversPlanning;

DELIMITER //

CREATE PROCEDURE prcDriversPlanning( IN param_driverId INTEGER )
BEGIN

    DECLARE dclTruckLicensePlate VARCHAR(16);
    DECLARE dclRestWeightKg FLOAT DEFAULT -1;
    DECLARE dclRestVolumeMet FLOAT DEFAULT -1;
    DECLARE dclCurrentCity VARCHAR(512);
    DECLARE dclCurrentTerminalType VARCHAR(128);

    DECLARE dclCurrentOrderId INTEGER;
    DECLARE dclCurrentWeightKg FLOAT;
    DECLARE dclCurrentVolumeMet FLOAT;

    DECLARE done INTEGER DEFAULT 0;
    DECLARE curPlan CURSOR FOR
        SELECT
            wor.orderId
            , ord.lengthMet * ord.widthMet * ord.heightMet AS volumeMet
            , wor.weightKg
        FROM vWeightOrderRank AS wor
        INNER JOIN tOrders AS ord
            ON ord.id = wor.orderId
        INNER JOIN vNonAssignedOrders AS nao
            ON nao.orderId = wor.orderId
        INNER JOIN tTerminals AS term
            ON term.id = ord.terminalId
        WHERE ord.cityDestination = dclCurrentCity
            AND term.type = dclCurrentTerminalType
        ORDER BY
            orderRank ASC;
    DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
    
    SET dclTruckLicensePlate = (
        SELECT
            vtr.licensePlateNumber
        FROM vDrivers AS vdr
        INNER JOIN vTrucks AS vtr
            ON vdr.truckId = vtr.truckId
        WHERE driverId = param_driverId
    );
    SET dclCurrentCity = ( -- текущий город по первому заказу
        SELECT DISTINCT
            ord.cityDestination
        FROM tOrderTransporation AS tot
        INNER JOIN tOrders AS ord
            ON ord.id = tot.orderId
        INNER JOIN vTrucks AS vtr
            ON vtr.truckId = tot.truckId
        WHERE vtr.licensePlateNumber = dclTruckLicensePlate
    );
    IF (dclCurrentCity IS NULL) THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'SQL: НАЗНАЧТЕ ПЕРВЫЙ ЗАКАЗ ВОДИТЕЛЮ/ГРУЗОВИКУ';
    END IF;
    SET dclCurrentTerminalType = (
        SELECT DISTINCT
            term.type
        FROM tOrderTransporation AS tot
        INNER JOIN tOrders AS ord
            ON ord.id = tot.orderId
        INNER JOIN vTrucks AS vtr
            ON vtr.truckId = tot.truckId
        INNER JOIN tTerminals AS term
            ON term.id = ord.terminalId
        WHERE vtr.licensePlateNumber = dclTruckLicensePlate
    );
    SET dclRestWeightKg = ( -- получаем свободный вес
        SELECT
            vtr.payLoadKg - ord.weightKg
        FROM vTrucks AS vtr
        LEFT JOIN(
            SELECT
                tot.truckId
                , SUM(ord.weightKg) AS weightKg
            FROM tOrderTransporation AS tot
            INNER JOIN tOrders AS ord
                ON ord.id = tot.orderId
            GROUP BY
                tot.truckId
        ) AS ord
            ON ord.truckId = vtr.truckId
        WHERE vtr.licensePlateNumber = dclTruckLicensePlate
    );
    SET dclRestVolumeMet = ( -- получаем свободный объём
        SELECT
            (vtr.lengthMet * vtr.heightMet * vtr.widthMet) - ord.volumeMet
        FROM vTrucks AS vtr
        LEFT JOIN(
            SELECT
                tot.truckId
                , SUM(ord.lengthMet * ord.heightMet * ord.widthMet) AS volumeMet
            FROM tOrderTransporation AS tot
            INNER JOIN tOrders AS ord
                ON ord.id = tot.orderId
            GROUP BY
                tot.truckId
        ) AS ord
            ON ord.truckId = vtr.truckId
        WHERE vtr.licensePlateNumber = dclTruckLicensePlate
    );
    
    IF (dclRestWeightKg <= 0 OR dclRestVolumeMet <= 0) THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'SQL: ГРУЗОВИК ПЕРЕПОЛНЕН';
    END IF;
    
    OPEN curPlan;
    
    REPEAT

        FETCH curPlan INTO dclCurrentOrderId, dclCurrentVolumeMet, dclCurrentWeightKg;
        IF NOT done THEN
            IF (dclRestWeightKg - dclCurrentWeightKg > 0
                OR dclRestVolumeMet - dclCurrentVolumeMet > 0) THEN
            BEGIN
                SET dclRestWeightKg = dclRestWeightKg - dclCurrentWeightKg;
                SET dclRestVolumeMet = dclRestVolumeMet - dclCurrentVolumeMet;
                CALL prcAssignOrder(dclTruckLicensePlate, dclCurrentOrderId);
            END;
            END IF;
        END IF;
    UNTIL done END REPEAT;
    
    CLOSE curPlan;
    
END //


USE TRANSPORATION;

DROP PROCEDURE IF EXISTS prcUpdateOrderStatus;

DELIMITER //

CREATE PROCEDURE prcUpdateOrderStatus(
    IN param_orderId INTEGER
    , IN param_statusId INTEGER
)
BEGIN
    
    DECLARE orderCnt INTEGER DEFAULT 0;
    DECLARE driverId INTEGER;
    DECLARE driverStatusId INTEGER DEFAULT 1; -- 1 - статус "свободен" у водителя
    
    IF param_statusId IN (4, 5, 6, 7) THEN    
        INSERT INTO tOrderStatusHistory(
            orderId
            , orderName
            , terminalId
            , truckId
            , createDtm
            , statusId
            , startStatusDtm
        )
        SELECT
            ord.id AS orderId
            , ord.name AS orderName
            , ord.terminalId
            , tot.truckId
            , ord.createDtm
            , osc.statusId
            , osc.startDtm AS startStatusDtm
        FROM tOrders AS ord
        INNER JOIN tOrderStatusCurrent AS osc
            ON osc.orderId = ord.id
        INNER JOIN tOrderTransporation AS tot
            ON tot.orderId = ord.id
        WHERE ord.id = param_orderId;
            
        DELETE osc
        FROM tOrderStatusCurrent AS osc
        WHERE osc.orderId = param_orderId;
        
        INSERT INTO tOrderStatusCurrent(
            orderId
            , statusId
        )
        VALUES (param_orderId, param_statusId);
    ELSE
        INSERT INTO tOrderStatusHistory(
            orderId
            , orderName
            , terminalId
            , truckId
            , createDtm
            , statusId
            , startStatusDtm
        )
        SELECT
            ord.id AS orderId
            , ord.name AS orderName
            , ord.terminalId
            , tot.truckId
            , ord.createDtm
            , osc.statusId
            , osc.startDtm AS startStatusDtm
        FROM tOrders AS ord
        INNER JOIN tOrderStatusCurrent AS osc
            ON osc.orderId = ord.id
        INNER JOIN tOrderTransporation AS tot
            ON tot.orderId = ord.id
        WHERE ord.id = param_orderId
        UNION
        SELECT
            ord.id AS orderId
            , ord.name AS orderName
            , ord.terminalId
            , tot.truckId
            , ord.createDtm
            , param_statusId
            , CURRENT_TIMESTAMP() AS startStatusDtm
        FROM tOrders AS ord
        INNER JOIN tOrderTransporation AS tot
            ON tot.orderId = ord.id
        WHERE ord.id = param_orderId;
        
        SET driverId = (
            SELECT
                tdr.userId
            FROM tDrivers AS tdr
            INNER JOIN tOrderTransporation AS tot
                ON tdr.truckId = tot.truckId
            WHERE tot.orderId = param_orderId
        ); 
            
        DELETE osc
        FROM tOrderStatusCurrent AS osc
        WHERE osc.orderId = param_orderId;
        
        DELETE ord
        FROM tOrders AS ord
        WHERE ord.id = param_orderId;
        
        SET orderCnt = (
            SELECT
                CASE
                    WHEN COUNT(tot.orderId) > 0
                    THEN COUNT(tot.orderId)
                    ELSE 0
                END AS orderCnt
            FROM tDrivers AS tdr
            INNER JOIN tOrderTransporation AS tot
                ON tot.truckId = tdr.truckId
        );
        
        IF orderCnt = 0 THEN
            UPDATE tDrivers
            SET statusId = 1
                , startDtm = CURRENT_TIMESTAMP()
            WHERE userId = driverId;
        END IF;
    
        
    END IF;
    
END //

