USE TRANSPORATION;

INSERT INTO tRole(
    name
)
VALUES('Администратор панели')
    ,('Водитель')
    ,('Аналитик');


USE TRANSPORATION;

INSERT INTO tUsers(
    roleId
    , login
    , firstName
    , lastName
    , fathersName
    , 

    , email
)
VALUES(1,   'kornienko_ds', 'Корниенко','Денис',    'Сергеевич',    CAST('19980708' AS DATE),   'kornienko_ds@kornienkocorp.com')
    ,(2,    'masenko_ie',   'Масенко',  'Игорь',    'Евгеньевич',   CAST('19971205' AS DATE),   'masenko@kornienkocorp_dr.com')
    ,(2,    'drobot_va',    'Дробот',   'Василий',  'Александрович',CAST('19980823' AS DATE),   'drobot_va@kornienkocorp_dr.com')
    ,(3,    'ivanov_ii',    'Иванов',   'Иван',     'Ильич',        CAST('19900808' AS DATE),   'ivanov_ii@kornienkocorp.com')
    ,(2,    'spidorod_gp',  'Спидоров', 'Геннадий', 'Павлович',     CAST('19780921' AS DATE),   'spidorod_gp@kornienkocorp_dr.com');


USE TRANSPORATION;

INSERT INTO tTerminals(
    name
)
VALUES('Алкоголь','Алкоголь-1'), ('Алкоголь','Алкоголь-2'), ('Молочная_продукция','МП_1'), ('Молочная_продукция','МП_2'), ('Растительного_происхождения','РП_1'), ('Растительного_происхождения','РП_2'), 
    ('Растительного_происхождения','РП_3'),('Растительного_происхождения','РП_4'), ('Быто-химимческий', 'БХ-1'), ('Быто-химимческий', 'БХ-2'), ('Быто-химимческий', 'БХ-3'), ('Мясной', 'М-1'), ('Мясной', 'М-2')
    , ('Мясной', 'М-3'), ('Мясной', 'М-4');

USE TRANSPORATION;

INSERT INTO tStatusType(
    name
)
VALUES('Заказ'), ('Водитель'), ('Терминал');


USE TRANSPORATION;

INSERT INTO tStatus(
    typeId
    , name
)
VALUES(2, 'Свободен')
    ,(2, 'В работе')
    ,(1, 'Не назначен')
    ,(1, 'Назначен')
    ,(1, 'Осмотр')
    ,(1, 'Погрузка')
    ,(1, 'Погрузка окончена')
    ,(1, 'Доставлено')
    ,(3, 'Свободен')
    ,(3, 'Занят');

USE TRANSPORATION;

INSERT INTO tCredentials(
    userId
    , password
)
VALUES(1, 'GfvA4s6K1'), (2, '12345678'), (3, '12345678')
    ,(4, '12345678'), (5, '12345678');



USE TRANSPORATION;

-- цифры вбивались на обум, так что возможно их перебивать
INSERT INTO tTrucksModel(
    manufacturer
    , model
    , lengthMet
    , widthMet
    , heightMet
    , carWeightKg
    , maxVolumeMet
    , payLoadKg
)
VALUES('MAN', 'TGL', 5.870, 2.240, 2.531, 2000, 4.3, 8000);


USE TRANSPORATION;

INSERT INTO tTrucksLicense(
    modelId
    , licensePlateNumber
)
VALUES(1, 'A123AA123RUS')
    ,(1, 'E111EE777RUS')
    ,(1, 'B777OP66RUS')
    ,(1, 'X666AM55RUS')
    ,(1, 'A333BC01RUS')
    ,(1, 'T342RA61RUS')
    ,(1, 'H528OP123RUS');


USE TRANSPORATION;

INSERT INTO tDrivers(
    userId
    , truckId
    , statusId
)
VALUES(2, 2, 1)
,(3, 3, 1)
,(5, 1, 1);


USE TRANSPORATION;

INSERT INTO tOrders(
    name
    , terminalId
    , weightKg
    , lengthMet
    , widthMet
    , heightMet
    , cityDestination
    , addressDestination
)
VALUES('Кубанская бурёнка', 1, 34.5, 0.5, 0.5, 0.5, ,'Краснодар', 'ул. Совхозная, 123');
