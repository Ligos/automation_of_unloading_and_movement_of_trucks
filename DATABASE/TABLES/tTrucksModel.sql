USE TRANSPORATION;

-- таблица-справочник с моделями и показателями грузовиков
CREATE TABLE IF NOT EXISTS tTrucksModel(
    id              INTEGER         NOT NULL AUTO_INCREMENT -- id типа грузовика
    , manufacturer  VARCHAR(128)    NOT NULL -- наименование производителя
    , model         VARCHAR(128)    NOT NULL -- наименование модели
    , lengthMet     FLOAT           NOT NULL -- длина грузовка в метрах
    , widthMet      FLOAT           NOT NULL -- ширина грузовика в метрах
    , heightMet     FLOAT           NOT NULL -- высотка грузовки в метрах
    , carWeightKg   FLOAT               NULL -- вес автомобиля в килограммах
    , maxVolumeMet  FLOAT           NOT NULL -- максимальный объём груза в метрах кубических, что может взять грузовик
    , payLoadKg     FLOAT           NOT NULL -- вес полезного груза в кг
    , CONSTRAINT PK_TrucksModel PRIMARY KEY (id)
);


