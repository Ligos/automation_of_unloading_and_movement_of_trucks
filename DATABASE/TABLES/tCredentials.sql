USE TRANSPORATION;

-- таблица с данными для аутентификации пользовалей
CREATE TABLE IF NOT EXISTS tCredentials(
    userId      INTEGER     NOT NULL -- id пользователя
    , password  LONGTEXT    NOT NULL -- пароль пользователя
    , CONSTRAINT PK_Credentials PRIMARY KEY (userId)
);


