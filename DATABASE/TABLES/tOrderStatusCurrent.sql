USE TRANSPORATION;

-- таблица с текущим состоянием заказа
CREATE TABLE IF NOT EXISTS tOrderStatusCurrent(
    orderId             INTEGER     NOT NULL -- id заказа
    , statusId          INTEGER     NOT NULL -- id статуса заказа
    , startDtm          DATETIME    NOT NULL DEFAULT CURRENT_TIMESTAMP -- дата и время начала действия статуса
    , CONSTRAINT PK_OrderStatusCurrent PRIMARY KEY (orderId)
);


