USE TRANSPORATION;

DROP TABLE IF EXISTS tOrders;

-- таблица с заказами
CREATE TABLE IF NOT EXISTS tOrders(
    id                      INTEGER     NOT NULL AUTO_INCREMENT -- id заказа
    , name                  VARCHAR(512)NOT NULL -- наименование заказа
    , terminalId            INTEGER     NOT NULL -- id терминала, на котором располагается заказ
    , weightKg              FLOAT       NOT NULL -- вес заказа в кг
    , lengthMet             FLOAT       NOT NULL -- длина заказа в метрах
    , widthMet              FLOAT       NOT NULL -- ширина заказа в метрах
    , heightMet             FLOAT       NOT NULL -- высота заказа в метрах
    , cityDestination       VARCHAR(512)    NULL -- город назначения
    , addressDestination    VARCHAR(512)    NULL -- адрес назначения
    , createDtm             DATETIME    NOT NULL DEFAULT CURRENT_TIMESTAMP -- дата и время создания заказа
    , CONSTRAINT PK_Orders PRIMARY KEY (id)
);


