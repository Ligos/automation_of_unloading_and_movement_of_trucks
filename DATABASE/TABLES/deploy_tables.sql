USE TRANSPORATION;

-- таблица с данными для аутентификации пользовалей
CREATE TABLE IF NOT EXISTS tCredentials(
    userId      INTEGER     NOT NULL -- id пользователя
    , password  LONGTEXT    NOT NULL -- пароль пользователя
    , CONSTRAINT PK_Credentials PRIMARY KEY (userId)
);


USE TRANSPORATION;

-- таблица с привязкой человека и грузовика
CREATE TABLE IF NOT EXISTS tDrivers(
    userId      INTEGER     NOT NULL -- id пользователя
    , truckId   INTEGER     NOT NULL -- id грузовика
    , statusId  INTEGER     NOT NULL -- текущий статус водителя и грузовика
    , startDtm  DATETIME    NOT NULL DEFAULT CURRENT_TIMESTAMP -- дата и время начала действия статуса
    , CONSTRAINT PK_Drivers PRIMARY KEY (userId, truckId)
);


USE TRANSPORATION;

DROP TABLE IF EXISTS tOrders;

-- таблица с заказами
CREATE TABLE IF NOT EXISTS tOrders(
    id                      INTEGER     NOT NULL AUTO_INCREMENT -- id заказа
    , name                  VARCHAR(512)NOT NULL -- наименование заказа
    , terminalId            INTEGER     NOT NULL -- id терминала, на котором располагается заказ
    , weightKg              FLOAT       NOT NULL -- вес заказа в кг
    , lengthMet             FLOAT       NOT NULL -- длина заказа в метрах
    , widthMet              FLOAT       NOT NULL -- ширина заказа в метрах
    , heightMet             FLOAT       NOT NULL -- высота заказа в метрах
    , cityDestination       VARCHAR(512)    NULL -- город назначения
    , addressDestination    VARCHAR(512)    NULL -- адрес назначения
    , createDtm             DATETIME    NOT NULL DEFAULT CURRENT_TIMESTAMP -- дата и время создания заказа
    , CONSTRAINT PK_Orders PRIMARY KEY (id)
);


USE TRANSPORATION;

-- таблица с текущим состоянием заказа
CREATE TABLE IF NOT EXISTS tOrderStatusCurrent(
    orderId             INTEGER     NOT NULL -- id заказа
    , statusId          INTEGER     NOT NULL -- id статуса заказа
    , startDtm          DATETIME    NOT NULL DEFAULT CURRENT_TIMESTAMP -- дата и время начала действия статуса
    , CONSTRAINT PK_OrderStatusCurrent PRIMARY KEY (orderId)
);


USE TRANSPORATION;

-- таблица с историей состояний заказа
CREATE TABLE IF NOT EXISTS tOrderStatusHistory(
    orderId                 INTEGER     NOT NULL -- id заказа
    , orderName             VARCHAR(512)NOT NULL -- наименование заказа
    , terminalId            INTEGER     NOT NULL -- id терминала
    , truckId               INTEGER     NOT NULL -- id грузовика
    , createDtm             DATETIME    NOT NULL -- дата и время создания заявки
    , statusId              INTEGER     NOT NULL -- статус заказа
    , startStatusDtm        DATETIME    NOT NULL -- дата и время назначения статуса заказа водителю/грузовику
    , completeStatusDtm     DATETIME    NOT NULL DEFAULT CURRENT_TIMESTAMP -- дата и время окончания статуса заказа
);


USE TRANSPORATION;

-- таблица с заказом и привязанным к нему грузовиком
CREATE TABLE IF NOT EXISTS tOrderTransporation(
    orderId     INTEGER NOT NULL -- id заказа
    , truckId   INTEGER NOT NULL -- id грузовика
    , CONSTRAINT PK_OrderTransporation PRIMARY KEY (orderId)
);


USE TRANSPORATION;

-- таблица-справочник ролей пользователей
CREATE TABLE IF NOT EXISTS tRole(
    id      INTEGER         NOT NULL AUTO_INCREMENT -- id роли
    , name  VARCHAR(128)    NOT NULL -- наименование роли
    , CONSTRAINT PK_Role PRIMARY KEY (id)
);


USE TRANSPORATION;

-- таблица-справочник статусов
CREATE TABLE IF NOT EXISTS tStatus(
    id          INTEGER         NOT NULL AUTO_INCREMENT -- id статуса
    , typeId    SMALLINT        NOT NULL -- id тип статуса
    , name      VARCHAR(128)    NOT NULL -- наименование статуса
    , CONSTRAINT PK_Status PRIMARY KEY (id)
);


USE TRANSPORATION;

-- таблица-справочник с типами статуса
CREATE TABLE IF NOT EXISTS tStatusType(
    id      SMALLINT        NOT NULL AUTO_INCREMENT -- id типа статуса
    , name  VARCHAR(128)    NOT NULL -- кому относится статус
    , CONSTRAINT PK_StatusType PRIMARY KEY (id)
);


USE TRANSPORATION;

-- таблица-справочник по терминалам
CREATE TABLE IF NOT EXISTS tTerminals(
    id      INTEGER         NOT NULL AUTO_INCREMENT -- id терминала
    , type  VARCHAR(128) 	NOT NULL -- тип терминала
    , name  VARCHAR(128)    NOT NULL -- наименование терминала
    , CONSTRAINT PK_Terminals PRIMARY KEY (id)
);


USE TRANSPORATION;

-- таблица со статусами терминала (текущие и прошедшие)
CREATE TABLE IF NOT EXISTS tTerminalStatus(
    terminalId  INTEGER     NOT NULL -- id терминала
    , statusId  INTEGER     NOT NULL -- id статуса
    , startDtm  DATETIME    NOT NULL DEFAULT CURRENT_TIMESTAMP -- начала действия статуса
    , CONSTRAINT PK_TerminalStatus PRIMARY KEY (terminalId, startDtm)
);


USE TRANSPORATION;

-- таблица-справочник с грузовиками, имеющиеся у компании
CREATE TABLE IF NOT EXISTS tTrucksLicense(
    truckId                 INTEGER     NOT NULL AUTO_INCREMENT -- id грузовика
    , modelId               INTEGER     NOT NULL -- id модели грузовика
    , licensePlateNumber    VARCHAR(16) NOT NULL -- номерные знаки грузовика
    , CONSTRAINT PK_TrucksLicense PRIMARY KEY (truckId)
);


USE TRANSPORATION;

-- таблица-справочник с моделями и показателями грузовиков
CREATE TABLE IF NOT EXISTS tTrucksModel(
    id              INTEGER         NOT NULL AUTO_INCREMENT -- id типа грузовика
    , manufacturer  VARCHAR(128)    NOT NULL -- наименование производителя
    , model         VARCHAR(128)    NOT NULL -- наименование модели
    , lengthMet     FLOAT           NOT NULL -- длина грузовка в метрах
    , widthMet      FLOAT           NOT NULL -- ширина грузовика в метрах
    , heightMet     FLOAT           NOT NULL -- высотка грузовки в метрах
    , carWeightKg   FLOAT               NULL -- вес автомобиля в килограммах
    , maxVolumeMet  FLOAT           NOT NULL -- максимальный объём груза в метрах кубических, что может взять грузовик
    , payLoadKg     FLOAT           NOT NULL -- вес полезного груза в кг
    , CONSTRAINT PK_TrucksModel PRIMARY KEY (id)
);


USE TRANSPORATION;

-- таблица-справочник пользователей
CREATE TABLE IF NOT EXISTS tUsers(
    id              INTEGER         NOT NULL AUTO_INCREMENT -- id пользователя
    , roleId        INTEGER         NOT NULL -- id роли
    , login         VARCHAR(128)    NOT NULL -- логин
    , firstName     VARCHAR(128)    NOT NULL -- имя
    , lastName      VARCHAR(128)    NOT NULL -- фамилия
    , fathersName   VARCHAR(128)        NULL -- отчество
    , birthday      DATE                NULL -- дата рождения
    , phoneNumber   VARCHAR(14)         NULL -- номер телефона
    , email         VARCHAR(128)    NOT NULL -- корпоративная почта
    , CONSTRAINT PK_Users PRIMARY KEY (id)
);

