USE TRANSPORATION;

-- таблица с привязкой человека и грузовика
CREATE TABLE IF NOT EXISTS tDrivers(
    userId      INTEGER     NOT NULL -- id пользователя
    , truckId   INTEGER     NOT NULL -- id грузовика
    , statusId  INTEGER     NOT NULL -- текущий статус водителя и грузовика
    , startDtm  DATETIME    NOT NULL DEFAULT CURRENT_TIMESTAMP -- дата и время начала действия статуса
    , CONSTRAINT PK_Drivers PRIMARY KEY (userId, truckId)
);


