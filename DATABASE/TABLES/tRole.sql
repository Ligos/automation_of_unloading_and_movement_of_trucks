USE TRANSPORATION;

-- таблица-справочник ролей пользователей
CREATE TABLE IF NOT EXISTS tRole(
    id      INTEGER         NOT NULL AUTO_INCREMENT -- id роли
    , name  VARCHAR(128)    NOT NULL -- наименование роли
    , CONSTRAINT PK_Role PRIMARY KEY (id)
);


