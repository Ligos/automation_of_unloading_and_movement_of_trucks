USE TRANSPORATION;

-- таблица со статусами терминала (текущие и прошедшие)
CREATE TABLE IF NOT EXISTS tTerminalStatus(
    terminalId  INTEGER     NOT NULL -- id терминала
    , statusId  INTEGER     NOT NULL -- id статуса
    , startDtm  DATETIME    NOT NULL DEFAULT CURRENT_TIMESTAMP -- начала действия статуса
    , CONSTRAINT PK_TerminalStatus PRIMARY KEY (terminalId, startDtm)
);


