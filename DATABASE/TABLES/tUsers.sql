USE TRANSPORATION;

-- таблица-справочник пользователей
CREATE TABLE IF NOT EXISTS tUsers(
    id              INTEGER         NOT NULL AUTO_INCREMENT -- id пользователя
    , roleId        INTEGER         NOT NULL -- id роли
    , login         VARCHAR(128)    NOT NULL -- логин
    , firstName     VARCHAR(128)    NOT NULL -- имя
    , lastName      VARCHAR(128)    NOT NULL -- фамилия
    , fathersName   VARCHAR(128)        NULL -- отчество
    , birthday      DATE                NULL -- дата рождения
    , phoneNumber   VARCHAR(14)         NULL -- номер телефона
    , email         VARCHAR(128)    NOT NULL -- корпоративная почта
    , CONSTRAINT PK_Users PRIMARY KEY (id)
);

