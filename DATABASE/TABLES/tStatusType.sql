USE TRANSPORATION;

-- таблица-справочник с типами статуса
CREATE TABLE IF NOT EXISTS tStatusType(
    id      SMALLINT        NOT NULL AUTO_INCREMENT -- id типа статуса
    , name  VARCHAR(128)    NOT NULL -- кому относится статус
    , CONSTRAINT PK_StatusType PRIMARY KEY (id)
);


