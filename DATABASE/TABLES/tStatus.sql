USE TRANSPORATION;

-- таблица-справочник статусов
CREATE TABLE IF NOT EXISTS tStatus(
    id          INTEGER         NOT NULL AUTO_INCREMENT -- id статуса
    , typeId    SMALLINT        NOT NULL -- id тип статуса
    , name      VARCHAR(128)    NOT NULL -- наименование статуса
    , CONSTRAINT PK_Status PRIMARY KEY (id)
);


