USE TRANSPORATION;

-- таблица-справочник с грузовиками, имеющиеся у компании
CREATE TABLE IF NOT EXISTS tTrucksLicense(
    truckId                 INTEGER     NOT NULL AUTO_INCREMENT -- id грузовика
    , modelId               INTEGER     NOT NULL -- id модели грузовика
    , licensePlateNumber    VARCHAR(16) NOT NULL -- номерные знаки грузовика
    , CONSTRAINT PK_TrucksLicense PRIMARY KEY (truckId)
);


