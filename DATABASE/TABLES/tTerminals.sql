USE TRANSPORATION;

-- таблица-справочник по терминалам
CREATE TABLE IF NOT EXISTS tTerminals(
    id      INTEGER         NOT NULL AUTO_INCREMENT -- id терминала
    , type  VARCHAR(128) 	NOT NULL -- тип терминала
    , name  VARCHAR(128)    NOT NULL -- наименование терминала
    , CONSTRAINT PK_Terminals PRIMARY KEY (id)
);


