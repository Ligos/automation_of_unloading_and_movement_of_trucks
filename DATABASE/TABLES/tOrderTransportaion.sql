USE TRANSPORATION;

-- таблица с заказом и привязанным к нему грузовиком
CREATE TABLE IF NOT EXISTS tOrderTransporation(
    orderId     INTEGER NOT NULL -- id заказа
    , truckId   INTEGER NOT NULL -- id грузовика
    , CONSTRAINT PK_OrderTransporation PRIMARY KEY (orderId)
);


