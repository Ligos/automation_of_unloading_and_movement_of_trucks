USE TRANSPORATION;

-- таблица с историей состояний заказа
CREATE TABLE IF NOT EXISTS tOrderStatusHistory(
    orderId                 INTEGER     NOT NULL -- id заказа
    , orderName             VARCHAR(512)NOT NULL -- наименование заказа
    , terminalId            INTEGER     NOT NULL -- id терминала
    , truckId               INTEGER     NOT NULL -- id грузовика
    , createDtm             DATETIME    NOT NULL -- дата и время создания заявки
    , statusId              INTEGER     NOT NULL -- статус заказа
    , startStatusDtm        DATETIME    NOT NULL -- дата и время назначения статуса заказа водителю/грузовику
    , completeStatusDtm     DATETIME    NOT NULL DEFAULT CURRENT_TIMESTAMP -- дата и время окончания статуса заказа
);


