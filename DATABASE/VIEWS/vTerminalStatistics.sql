USE TRANSPORATION;

CREATE OR REPLACE VIEW vTerminalStatistics
AS
SELECT
    term.id AS terminalId
    , term.type AS terminalType
    , term.name AS terminalName
    , COUNT(DISTINCT osh.orderId) AS ordersHistoryCnt -- кол-во обработанных заказов
    , COUNT(DISTINCT osc.orderId) AS orderCurrentCnt -- кол-во текущий заявок в очереди
    , ROUND(AVG(oshQueue.queueTimeSec) / 60, 2) AS avgQueueTimeMin -- среднее время заказа в очереди в минутах
    , ROUND(AVG(oshLoading.loadingTimeSec) / 60, 2)  AS avgLoadingTimeMin -- среднее время погрузки в минутах
FROM tTerminals AS term
LEFT JOIN tOrderStatusHistory AS osh
    ON term.id = osh.terminalId
LEFT JOIN (
    SELECT
        orderId
        , terminalId
        , TIMESTAMPDIFF(SECOND, startStatusDtm, completeStatusDtm) AS queueTimeSec
    FROM tOrderStatusHistory
    WHERE statusId = 5
) AS oshQueue
    ON term.id = oshQueue.terminalId
    AND osh.orderId = oshQueue.orderId
    AND osh.statusId = 5
LEFT JOIN (
    SELECT
        orderId
        , terminalId
        , TIMESTAMPDIFF(SECOND, startStatusDtm, completeStatusDtm) AS loadingTimeSec
    FROM tOrderStatusHistory
    WHERE statusId = 6
) AS oshLoading
    ON term.id = oshLoading.terminalId
    AND osh.orderId = oshLoading.orderId
    AND osh.statusId = 6
LEFT JOIN tOrders AS tord
    ON term.id = tord.terminalId
LEFT JOIN tOrderStatusCurrent AS osc
    ON tord.id = osc.orderId
    AND osc.statusId IN (3, 4, 5, 6) -- не назначен, назначен, осмотр, погрузка
GROUP BY
    osh.terminalId
    , term.name;

