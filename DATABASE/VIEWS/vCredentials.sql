USE TRANSPORATION;

CREATE OR REPLACE VIEW vCredentials
AS
SELECT
    us.login
    , credit.password
FROM tCredentials AS credit
INNER JOIN tUsers AS us
    ON credit.userId = us.id;


