USE TRANSPORATION;

CREATE OR REPLACE VIEW vAssignedOrders
AS
SELECT
    osc.orderId
    , ord.name AS orderName
    , osc.startDtm
    , osc.statusId
    , ts.name AS statusName
    , ord.terminalId
    , term.name AS terminalName
    , ord.weightKg
    , ord.lengthMet
    , ord.widthMet
    , ord.heightMet
    , CONCAT(ord.cityDestination, ord.addressDestination) AS destination
    , ord.createDtm
FROM tOrderStatusCurrent AS osc 
INNER JOIN tOrders AS ord
    ON osc.orderId = ord.id
INNER JOIN tTerminals AS term
    ON ord.terminalId = term.id
INNER JOIN tStatus AS ts
    ON ts.id = osc.statusId
WHERE osc.statusId BETWEEN 4 AND 7;



USE TRANSPORATION;

CREATE OR REPLACE VIEW vCredentials
AS
SELECT
    us.login
    , credit.password
FROM tCredentials AS credit
INNER JOIN tUsers AS us
    ON credit.userId = us.id;


USE TRANSPORATION;

CREATE OR REPLACE VIEW vNonAssignedOrders
AS
SELECT
    ord.id AS orderId
    , ord.name AS orderName
    , ord.terminalId
    , term.name AS terminalName
    , ord.weightKg
    , ord.lengthMet
    , ord.widthMet
    , ord.heightMet
    , CONCAT(ord.cityDestination, ord.addressDestination) AS destination
    , ord.createDtm
FROM tOrders AS ord
LEFT JOIN tOrderStatusCurrent AS osc
    ON osc.orderId = ord.id
INNER JOIN tTerminals AS term
    ON ord.terminalId = term.id
WHERE osc.orderId IS NULL
    OR osc.statusId = 3; -- статус "не назначен" для заказа


USE TRANSPORATION;

CREATE OR REPLACE VIEW vStatus
AS
SELECT
    tst.id AS typeId
    , tst.name AS typeName
    , ts.id AS statusId
    , ts.name AS statusName
FROM tStatus AS ts
INNER JOIN tStatusType AS tst
    ON tst.id = ts.typeId;

USE TRANSPORATION;

CREATE OR REPLACE VIEW vTerminalStatistics
AS
SELECT
    term.id AS terminalId
    , term.name AS terminalName
    , COUNT(DISTINCT osh.orderId) AS ordersHistoryCnt -- кол-во обработанных заказов
    , COUNT(DISTINCT osc.orderId) AS orderCurrentCnt -- кол-во текущий заявок в очереди
    , ROUND(AVG(oshQueue.queueTimeSec) / 60, 2) AS avgQueueTimeMin -- среднее время заказа в очереди в минутах
    , ROUND(AVG(oshLoading.loadingTimeSec) / 60, 2)  AS avgLoadingTimeMin -- среднее время погрузки в минутах
FROM tTerminals AS term
LEFT JOIN tOrderStatusHistory AS osh
    ON term.id = osh.terminalId
LEFT JOIN (
    SELECT
        orderId
        , terminalId
        , TIMESTAMPDIFF(SECOND, startStatusDtm, completeStatusDtm) AS queueTimeSec
    FROM tOrderStatusHistory
    WHERE statusId = 5
) AS oshQueue
    ON term.id = oshQueue.terminalId
    AND osh.orderId = oshQueue.orderId
    AND osh.statusId = 5
LEFT JOIN (
    SELECT
        orderId
        , terminalId
        , TIMESTAMPDIFF(SECOND, startStatusDtm, completeStatusDtm) AS loadingTimeSec
    FROM tOrderStatusHistory
    WHERE statusId = 6
) AS oshLoading
    ON term.id = oshLoading.terminalId
    AND osh.orderId = oshLoading.orderId
    AND osh.statusId = 6
LEFT JOIN tOrders AS tord
    ON term.id = tord.terminalId
LEFT JOIN tOrderStatusCurrent AS osc
    ON tord.id = osc.orderId
    AND osc.statudId IN (3, 4, 5, 6) -- не назначен, назначен, осмотр, погрузка
GROUP BY
    osh.terminalId
    , term.name;

USE TRANSPORATION;

CREATE OR REPLACE VIEW vTrucks
AS
SELECT
    tl.truckId
    , tm.manufacturer
    , tm.model
    , tm.lengthMet
    , tm.widthMet
    , tm.heightMet
    , tm.carWeightKg
    , tm.maxVolumeMet
    , tm.payLoadKg
    , tl.licensePlateNumber
FROM tTrucksLicense AS tl
INNER JOIN tTrucksModel AS tm
    ON tl.modelId = tm.id;



USE TRANSPORATION;

CREATE OR REPLACE VIEW vDrivers
AS
SELECT
    dr.userId AS driverId
    , us.login
    , us.firstName
    , us.lastName
    , us.fathersName
    , us.phoneNumber
    , dr.statusId
    , dr.truckId
    , tr.licensePlateNumber
FROM(
    SELECT
        userId
        , truckId
        , statusId
        , MAX(startDtm) AS startDtm
    FROM tDrivers
    GROUP BY
        userId
        , truckId
        , statusId
) AS dr
INNER JOIN tUsers AS us
    ON dr.userId = us.id
INNER JOIN vTrucks AS tr
    ON tr.truckId = dr.truckId;



USE TRANSPORATION;

CREATE OR REPLACE VIEW vWeightOrderRank
AS
SELECT
    ord.id AS orderId
    , ord.name AS orderName
    , term.id AS terminalId
    , term.type AS terminalType 
    , vdr.driverId
    , ord.weightKg
    , ord.cityDestination
    , ROW_NUMBER() OVER (
        PARTITION BY
            ord.cityDestination
            , term.type
        ORDER BY
            ord.weightKg DESC
            , vts.avgQueueTimeMin ASC
            , vts.avgLoadingTimeMin ASC
    ) AS orderRank
FROM tOrders AS ord
INNER JOIN tTerminals AS term
    ON ord.terminalId = term.id
LEFT JOIN vTerminalStatistics AS vts
    ON vts.terminalId = ord.terminalId
LEFT JOIN tOrderTransporation AS tot
    ON tot.orderId = ord.id
LEFT JOIN vDrivers AS vdr
    ON vdr.truckId = tot.truckId;


