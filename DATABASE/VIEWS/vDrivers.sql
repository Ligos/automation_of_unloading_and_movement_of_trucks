USE TRANSPORATION;

CREATE OR REPLACE VIEW vDrivers
AS
SELECT
    dr.userId AS driverId
    , us.login
    , us.firstName
    , us.lastName
    , us.fathersName
    , us.phoneNumber
    , dr.statusId
    , dr.truckId
    , tr.licensePlateNumber
FROM(
    SELECT
        userId
        , truckId
        , statusId
        , MAX(startDtm) AS startDtm
    FROM tDrivers
    GROUP BY
        userId
        , truckId
        , statusId
) AS dr
INNER JOIN tUsers AS us
    ON dr.userId = us.id
INNER JOIN vTrucks AS tr
    ON tr.truckId = dr.truckId;


