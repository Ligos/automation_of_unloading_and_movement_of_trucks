USE TRANSPORATION;

CREATE OR REPLACE VIEW vWeightOrderRank
AS
SELECT
    ord.id AS orderId
    , ord.name AS orderName
    , term.id AS terminalId
    , term.type AS terminalType 
    , vdr.driverId
    , ord.weightKg
    , ord.cityDestination
    , ROW_NUMBER() OVER (
        PARTITION BY
            ord.cityDestination
            , term.type
        ORDER BY
            ord.weightKg DESC
            , vts.avgQueueTimeMin ASC
            , vts.avgLoadingTimeMin ASC
    ) AS orderRank
FROM tOrders AS ord
INNER JOIN tTerminals AS term
    ON ord.terminalId = term.id
LEFT JOIN vTerminalStatistics AS vts
    ON vts.terminalId = ord.terminalId
LEFT JOIN tOrderTransporation AS tot
    ON tot.orderId = ord.id
LEFT JOIN vDrivers AS vdr
    ON vdr.truckId = tot.truckId;


