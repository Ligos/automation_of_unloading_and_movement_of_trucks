USE TRANSPORATION;

CREATE OR REPLACE VIEW vNonAssignedOrders
AS
SELECT
    ord.id AS orderId
    , ord.name AS orderName
    , ord.terminalId
    , term.name AS terminalName
    , ord.weightKg
    , ord.lengthMet
    , ord.widthMet
    , ord.heightMet
    , CONCAT_WS(' ', 'г.', ord.cityDestination, ord.addressDestination) AS destination
    , ord.createDtm
FROM tOrders AS ord
LEFT JOIN tOrderStatusCurrent AS osc
    ON osc.orderId = ord.id
INNER JOIN tTerminals AS term
    ON ord.terminalId = term.id
WHERE osc.orderId IS NULL
    OR osc.statusId = 3; -- статус "не назначен" для заказа


