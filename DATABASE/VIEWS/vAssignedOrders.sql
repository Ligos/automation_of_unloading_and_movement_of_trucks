USE TRANSPORATION;

CREATE OR REPLACE VIEW vAssignedOrders
AS
SELECT
    osc.orderId
    , ord.name AS orderName
    , osc.startDtm
    , osc.statusId
    , ts.name AS statusName
    , ord.terminalId
    , term.name AS terminalName
    , ord.weightKg
    , ord.lengthMet
    , ord.widthMet
    , ord.heightMet
    , CONCAT_WS(' ', 'г.', ord.cityDestination, ord.addressDestination) AS destination
    , ord.createDtm
FROM tOrderStatusCurrent AS osc 
INNER JOIN tOrders AS ord
    ON osc.orderId = ord.id
INNER JOIN tTerminals AS term
    ON ord.terminalId = term.id
INNER JOIN tStatus AS ts
    ON ts.id = osc.statusId
WHERE osc.statusId BETWEEN 4 AND 7;



