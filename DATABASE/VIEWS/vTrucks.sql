USE TRANSPORATION;

CREATE OR REPLACE VIEW vTrucks
AS
SELECT
    tl.truckId
    , tm.manufacturer
    , tm.model
    , tm.lengthMet
    , tm.widthMet
    , tm.heightMet
    , tm.carWeightKg
    , tm.maxVolumeMet
    , tm.payLoadKg
    , tl.licensePlateNumber
FROM tTrucksLicense AS tl
INNER JOIN tTrucksModel AS tm
    ON tl.modelId = tm.id;


