-- пользователь для создания подключения к БД из сервера
CREATE USER transporation_user_connection@localhost IDENTIFIED BY 'connectionuser';

GRANT SELECT ON TRANSPORATION.* TO transporation_user_connection@localhost;

-- некоторые справочники ему изменять нельзя

GRANT UPDATE ON TRANSPORATION.tOrders TO transporation_user_connection@localhost;
GRANT UPDATE ON TRANSPORATION.tOrderStatusCurrent TO transporation_user_connection@localhost;
GRANT UPDATE ON TRANSPORATION.tOrderStatusHistory TO transporation_user_connection@localhost;
GRANT UPDATE ON TRANSPORATION.tOrderTransporation TO transporation_user_connection@localhost;
GRANT UPDATE ON TRANSPORATION.tTerminalStatus TO transporation_user_connection@localhost;
GRANT UPDATE ON TRANSPORATION.tDrivers TO transporation_user_connection@localhost;
GRANT UPDATE ON TRANSPORATION.tUsers TO transporation_user_connection@localhost;
GRANT UPDATE ON TRANSPORATION.tCredentials TO transporation_user_connection@localhost;

GRANT INSERT ON TRANSPORATION.tOrders TO transporation_user_connection@localhost;
GRANT INSERT ON TRANSPORATION.tOrderStatusCurrent TO transporation_user_connection@localhost;
GRANT INSERT ON TRANSPORATION.tOrderStatusHistory TO transporation_user_connection@localhost;
GRANT INSERT ON TRANSPORATION.tOrderTransporation TO transporation_user_connection@localhost;
GRANT INSERT ON TRANSPORATION.tTerminalStatus TO transporation_user_connection@localhost;
GRANT INSERT ON TRANSPORATION.tDrivers TO transporation_user_connection@localhost;
GRANT INSERT ON TRANSPORATION.tUsers TO transporation_user_connection@localhost;
GRANT INSERT ON TRANSPORATION.tCredentials TO transporation_user_connection@localhost;

GRANT DELETE ON TRANSPORATION.tOrders TO transporation_user_connection@localhost;
GRANT DELETE ON TRANSPORATION.tOrderStatusCurrent TO transporation_user_connection@localhost;
GRANT DELETE ON TRANSPORATION.tOrderStatusHistory TO transporation_user_connection@localhost;
GRANT DELETE ON TRANSPORATION.tOrderTransporation TO transporation_user_connection@localhost;
GRANT DELETE ON TRANSPORATION.tTerminalStatus TO transporation_user_connection@localhost;
GRANT DELETE ON TRANSPORATION.tDrivers TO transporation_user_connection@localhost;
GRANT DELETE ON TRANSPORATION.tCredentials TO transporation_user_connection@localhost;


