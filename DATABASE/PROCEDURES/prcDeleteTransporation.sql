USE TRANSPORATION;

DROP PROCEDURE IF EXISTS prcDeleteTransporation;

DELIMITER //

CREATE PROCEDURE prcDeleteTransporation( IN param_orderId INTEGER)
BEGIN

    DECLARE dclStatusId INTEGER DEFAULT 3; -- 3 - статус "не назначен" для заказа
    DECLARE dclDriverId INTEGER DEFAULT -1;
    DECLARE dclIsDriverFree TINYINT DEFAULT 0;
    
    SET dclDriverId = (
        SELECT
            vdr.driverId
        FROM tOrderTransporation AS tot
        INNER JOIN vDrivers AS vdr
            ON tot.truckId = vdr.truckId
        WHERE tot.orderId = param_orderId
    );
    
    DELETE tot
    FROM tOrderTransporation AS tot
    WHERE tot.orderId = param_orderId;

    UPDATE tOrderStatusCurrent AS osc
    SET osc.statusId = dclStatusId
        , osc.startDtm = CURRENT_TIMESTAMP()
    WHERE osc.orderId = param_orderId;
    
    SET dclIsDriverFree = (
        SELECT
            CASE
                WHEN COUNT(tot.orderId) IS NULL OR COUNT(tot.orderId) = 0
                THEN 1
                ELSE 0
            END
        FROM tOrderTransporation AS tot
        INNER JOIN vDrivers AS vdr
            ON vdr.truckId = tot.truckId
        WHERE vdr.driverId = dclDriverId
    );
    
    IF dclIsDriverFree = 1 THEN
        UPDATE tDrivers
        SET statusId = 1
            , startDtm = CURRENT_TIMESTAMP()
        WHERE userId = dclDriverId;
    END IF;
END //


