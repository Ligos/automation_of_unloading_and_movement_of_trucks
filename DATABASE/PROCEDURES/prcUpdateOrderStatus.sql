USE TRANSPORATION;

DROP PROCEDURE IF EXISTS prcUpdateOrderStatus;

DELIMITER //

CREATE PROCEDURE prcUpdateOrderStatus(
    IN param_orderId INTEGER
    , IN param_statusId INTEGER
)
BEGIN
    
    DECLARE orderCnt INTEGER DEFAULT 0;
    DECLARE driverId INTEGER;
    DECLARE driverStatusId INTEGER DEFAULT 1; -- 1 - статус "свободен" у водителя
    
    IF param_statusId IN (4, 5, 6, 7) THEN    
        INSERT INTO tOrderStatusHistory(
            orderId
            , orderName
            , terminalId
            , truckId
            , createDtm
            , statusId
            , startStatusDtm
        )
        SELECT
            ord.id AS orderId
            , ord.name AS orderName
            , ord.terminalId
            , tot.truckId
            , ord.createDtm
            , osc.statusId
            , osc.startDtm AS startStatusDtm
        FROM tOrders AS ord
        INNER JOIN tOrderStatusCurrent AS osc
            ON osc.orderId = ord.id
        INNER JOIN tOrderTransporation AS tot
            ON tot.orderId = ord.id
        WHERE ord.id = param_orderId;
            
        DELETE osc
        FROM tOrderStatusCurrent AS osc
        WHERE osc.orderId = param_orderId;
        
        INSERT INTO tOrderStatusCurrent(
            orderId
            , statusId
        )
        VALUES (param_orderId, param_statusId);
    ELSE
        INSERT INTO tOrderStatusHistory(
            orderId
            , orderName
            , terminalId
            , truckId
            , createDtm
            , statusId
            , startStatusDtm
        )
        SELECT
            ord.id AS orderId
            , ord.name AS orderName
            , ord.terminalId
            , tot.truckId
            , ord.createDtm
            , osc.statusId
            , osc.startDtm AS startStatusDtm
        FROM tOrders AS ord
        INNER JOIN tOrderStatusCurrent AS osc
            ON osc.orderId = ord.id
        INNER JOIN tOrderTransporation AS tot
            ON tot.orderId = ord.id
        WHERE ord.id = param_orderId
        UNION
        SELECT
            ord.id AS orderId
            , ord.name AS orderName
            , ord.terminalId
            , tot.truckId
            , ord.createDtm
            , param_statusId
            , CURRENT_TIMESTAMP() AS startStatusDtm
        FROM tOrders AS ord
        INNER JOIN tOrderTransporation AS tot
            ON tot.orderId = ord.id
        WHERE ord.id = param_orderId;
        
        SET driverId = (
            SELECT
                tdr.userId
            FROM tDrivers AS tdr
            INNER JOIN tOrderTransporation AS tot
                ON tdr.truckId = tot.truckId
            WHERE tot.orderId = param_orderId
        ); 
            
        DELETE osc
        FROM tOrderStatusCurrent AS osc
        WHERE osc.orderId = param_orderId;
        
        DELETE ord
        FROM tOrders AS ord
        WHERE ord.id = param_orderId;
        
        SET orderCnt = (
            SELECT
                CASE
                    WHEN COUNT(tot.orderId) > 0
                    THEN COUNT(tot.orderId)
                    ELSE 0
                END AS orderCnt
            FROM tDrivers AS tdr
            INNER JOIN tOrderTransporation AS tot
                ON tot.truckId = tdr.truckId
        );
        
        IF orderCnt = 0 THEN
            UPDATE tDrivers
            SET statusId = 1
                , startDtm = CURRENT_TIMESTAMP()
            WHERE userId = driverId;
        END IF;
    
        
    END IF;
    
END //

