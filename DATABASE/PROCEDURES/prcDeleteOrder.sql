USE TRANSPORATION;

DROP PROCEDURE IF EXISTS prcDeleteOrder;

DELIMITER //

CREATE PROCEDURE prcDeleteOrder (IN param_orderId INTEGER)
BEGIN
    DELETE
    FROM tOrderStatusCurrent
    WHERE orderId = param_orderId;
    
    DELETE
    FROM tOrders
    WHERE id = param_orderId;
END //

