USE TRANSPORATION;

DROP PROCEDURE IF EXISTS prcAssignOrder;

DELIMITER // 

CREATE PROCEDURE prcAssignOrder(
    IN param_truckLicense VARCHAR(16)
    , IN param_orderId INTEGER
)
BEGIN

    DECLARE truckId INTEGER;
    DECLARE orderStatusId INTEGER DEFAULT 4; -- 4 статус "назначен" для заказа
    DECLARE driverStatusId INTEGER DEFAULT 2; -- 2 статус "в работе" для водителя
    DECLARE driverId INTEGER;
    DECLARE dclCurrentStatus INTEGER DEFAULT -1;
    
    SET truckId = (SELECT ttl.truckId FROM tTrucksLicense AS ttl WHERE LOWER(ttl.licensePlateNumber) = LOWER(param_truckLicense));
    SET driverId = (SELECT tdr.userId FROM tDrivers AS tdr WHERE tdr.truckId = truckId);
    SET dclCurrentStatus = (SELECT statusId FROM tOrderStatusCurrent WHERE orderId = param_orderId);
    
    INSERT INTO tOrderTransporation(
        orderId
        , truckId
    )
    VALUES (param_orderId, truckId);
    
    IF (dclCurrentStatus = -1 OR dclCurrentStatus IS NULL) THEN

        INSERT INTO tOrderStatusCurrent(
            orderId
            , statusId
        )
        VALUES(param_orderId, orderStatusId);

    ELSEIF (dclCurrentStatus = 3) THEN

        UPDATE tOrderStatusCurrent
        SET statusId = 4
            ,startDtm = CURRENT_TIMESTAMP()
        WHERE orderId = param_orderId;

    END IF;
        
    UPDATE tDrivers
    SET statusId = driverStatusId
        , startDtm = CURRENT_TIMESTAMP()
    WHERE userId = driverId
        AND statusId <> 2;

END //


