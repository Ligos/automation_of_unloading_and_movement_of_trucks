USE TRANSPORATION;

DROP PROCEDURE IF EXISTS prcAssignOrder;

DELIMITER // 

CREATE PROCEDURE prcAssignOrder(
    IN param_truckLicense VARCHAR(16)
    , IN param_orderId INTEGER
)
BEGIN

    DECLARE truckId INTEGER;
    DECLARE orderStatusId INTEGER DEFAULT 4; -- 4 статус "назначен" для заказа
    DECLARE driverStatusId INTEGER DEFAULT 2; -- 2 статус "в работе" для водителя
    DECLARE driverId INTEGER;
    DECLARE dclCurrentStatus INTEGER DEFAULT -1;
    
    SET truckId = (SELECT ttl.truckId FROM tTrucksLicense AS ttl WHERE LOWER(ttl.licensePlateNumber) = LOWER(param_truckLicense));
    SET driverId = (SELECT tdr.userId FROM tDrivers AS tdr WHERE tdr.truckId = truckId);
    SET dclCurrentStatus = (SELECT statusId FROM tOrderStatusCurrent WHERE orderId = param_orderId);
    
    INSERT INTO tOrderTransporation(
        orderId
        , truckId
    )
    VALUES (param_orderId, truckId);
    
    IF (dclCurrentStatus = -1 OR dclCurrentStatus IS NULL) THEN

        INSERT INTO tOrderStatusCurrent(
            orderId
            , statusId
        )
        VALUES(param_orderId, orderStatusId);

    ELSEIF (dclCurrentStatus = 3) THEN

        UPDATE tOrderStatusCurrent
        SET statusId = 4
            ,startDtm = CURRENT_TIMESTAMP()
        WHERE orderId = param_orderId;

    END IF;
        
    UPDATE tDrivers
    SET statusId = driverStatusId
        , startDtm = CURRENT_TIMESTAMP()
    WHERE userId = driverId
        AND statusId <> 2;

END //


USE TRANSPORATION;

DROP PROCEDURE IF EXISTS prcDeleteOrder;

DELIMITER //

CREATE PROCEDURE prcDeleteOrder (IN param_orderId INTEGER)
BEGIN
    DELETE
    FROM tOrderStatusCurrent
    WHERE orderId = param_orderId;
    
    DELETE
    FROM tOrders
    WHERE id = param_orderId;
END //

USE TRANSPORATION;

DROP PROCEDURE IF EXISTS prcDeleteTransporation;

DELIMITER //

CREATE PROCEDURE prcDeleteTransporation( IN param_orderId INTEGER)
BEGIN

    DECLARE dclStatusId INTEGER DEFAULT 3; -- 3 - статус "не назначен" для заказа
    DECLARE dclDriverId INTEGER DEFAULT -1;
    DECLARE dclIsDriverFree TINYINT DEFAULT 0;
    
    SET dclDriverId = (
        SELECT
            vdr.driverId
        FROM tOrderTransporation AS tot
        INNER JOIN vDrivers AS vdr
            ON tot.truckId = vdr.truckId
        WHERE tot.orderId = param_orderId
    );
    
    DELETE tot
    FROM tOrderTransporation AS tot
    WHERE tot.orderId = param_orderId;

    UPDATE tOrderStatusCurrent AS osc
    SET osc.statusId = dclStatusId
        , osc.startDtm = CURRENT_TIMESTAMP()
    WHERE osc.orderId = param_orderId;
    
    SET dclIsDriverFree = (
        SELECT
            CASE
                WHEN COUNT(tot.orderId) IS NULL OR COUNT(tot.orderId) = 0
                THEN 1
                ELSE 0
            END
        FROM tOrderTransporation AS tot
        INNER JOIN vDrivers AS vdr
            ON vdr.truckId = tot.truckId
        WHERE vdr.driverId = dclDriverId
    );
    
    IF dclIsDriverFree = 1 THEN
        UPDATE tDrivers
        SET statusId = 1
            , startDtm = CURRENT_TIMESTAMP()
        WHERE userId = dclDriverId;
    END IF;
END //


USE TRANSPORATION;

DROP PROCEDURE IF EXISTS prcDriversPlanning;

DELIMITER //

CREATE PROCEDURE prcDriversPlanning( IN param_driverId INTEGER )
BEGIN

    DECLARE dclTruckLicensePlate VARCHAR(16);
    DECLARE dclRestWeightKg FLOAT DEFAULT -1;
    DECLARE dclRestVolumeMet FLOAT DEFAULT -1;
    DECLARE dclCurrentCity VARCHAR(512);
    DECLARE dclCurrentTerminalType VARCHAR(128);

    DECLARE dclCurrentOrderId INTEGER;
    DECLARE dclCurrentWeightKg FLOAT;
    DECLARE dclCurrentVolumeMet FLOAT;

    DECLARE done INTEGER DEFAULT 0;
    DECLARE curPlan CURSOR FOR
        SELECT
            wor.orderId
            , ord.lengthMet * ord.widthMet * ord.heightMet AS volumeMet
            , wor.weightKg
        FROM vWeightOrderRank AS wor
        INNER JOIN tOrders AS ord
            ON ord.id = wor.orderId
        INNER JOIN vNonAssignedOrders AS nao
            ON nao.orderId = wor.orderId
        INNER JOIN tTerminals AS term
            ON term.id = ord.terminalId
        WHERE ord.cityDestination = dclCurrentCity
            AND term.type = dclCurrentTerminalType
        ORDER BY
            orderRank ASC;
    DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
    
    SET dclTruckLicensePlate = (
        SELECT
            vtr.licensePlateNumber
        FROM vDrivers AS vdr
        INNER JOIN vTrucks AS vtr
            ON vdr.truckId = vtr.truckId
        WHERE driverId = param_driverId
    );
    SET dclCurrentCity = ( -- текущий город по первому заказу
        SELECT DISTINCT
            ord.cityDestination
        FROM tOrderTransporation AS tot
        INNER JOIN tOrders AS ord
            ON ord.id = tot.orderId
        INNER JOIN vTrucks AS vtr
            ON vtr.truckId = tot.truckId
        WHERE vtr.licensePlateNumber = dclTruckLicensePlate
    );
    IF (dclCurrentCity IS NULL) THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'SQL: НАЗНАЧТЕ ПЕРВЫЙ ЗАКАЗ ВОДИТЕЛЮ/ГРУЗОВИКУ';
    END IF;
    SET dclCurrentTerminalType = (
        SELECT DISTINCT
            term.type
        FROM tOrderTransporation AS tot
        INNER JOIN tOrders AS ord
            ON ord.id = tot.orderId
        INNER JOIN vTrucks AS vtr
            ON vtr.truckId = tot.truckId
        INNER JOIN tTerminals AS term
            ON term.id = ord.terminalId
        WHERE vtr.licensePlateNumber = dclTruckLicensePlate
    );
    SET dclRestWeightKg = ( -- получаем свободный вес
        SELECT
            vtr.payLoadKg - ord.weightKg
        FROM vTrucks AS vtr
        LEFT JOIN(
            SELECT
                tot.truckId
                , SUM(ord.weightKg) AS weightKg
            FROM tOrderTransporation AS tot
            INNER JOIN tOrders AS ord
                ON ord.id = tot.orderId
            GROUP BY
                tot.truckId
        ) AS ord
            ON ord.truckId = vtr.truckId
        WHERE vtr.licensePlateNumber = dclTruckLicensePlate
    );
    SET dclRestVolumeMet = ( -- получаем свободный объём
        SELECT
            (vtr.lengthMet * vtr.heightMet * vtr.widthMet) - ord.volumeMet
        FROM vTrucks AS vtr
        LEFT JOIN(
            SELECT
                tot.truckId
                , SUM(ord.lengthMet * ord.heightMet * ord.widthMet) AS volumeMet
            FROM tOrderTransporation AS tot
            INNER JOIN tOrders AS ord
                ON ord.id = tot.orderId
            GROUP BY
                tot.truckId
        ) AS ord
            ON ord.truckId = vtr.truckId
        WHERE vtr.licensePlateNumber = dclTruckLicensePlate
    );
    
    IF (dclRestWeightKg <= 0 OR dclRestVolumeMet <= 0) THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'SQL: ГРУЗОВИК ПЕРЕПОЛНЕН';
    END IF;
    
    OPEN curPlan;
    
    REPEAT

        FETCH curPlan INTO dclCurrentOrderId, dclCurrentVolumeMet, dclCurrentWeightKg;
        IF NOT done THEN
            IF (dclRestWeightKg - dclCurrentWeightKg > 0
                OR dclRestVolumeMet - dclCurrentVolumeMet > 0) THEN
            BEGIN
                SET dclRestWeightKg = dclRestWeightKg - dclCurrentWeightKg;
                SET dclRestVolumeMet = dclRestVolumeMet - dclCurrentVolumeMet;
                CALL prcAssignOrder(dclTruckLicensePlate, dclCurrentOrderId);
            END;
            END IF;
        END IF;
    UNTIL done END REPEAT;
    
    CLOSE curPlan;
    
END //


USE TRANSPORATION;

DROP PROCEDURE IF EXISTS prcUpdateOrderStatus;

DELIMITER //

CREATE PROCEDURE prcUpdateOrderStatus(
    IN param_orderId INTEGER
    , IN param_statusId INTEGER
)
BEGIN
    
    DECLARE orderCnt INTEGER DEFAULT 0;
    DECLARE driverId INTEGER;
    DECLARE driverStatusId INTEGER DEFAULT 1; -- 1 - статус "свободен" у водителя
    
    IF param_statusId IN (4, 5, 6, 7) THEN    
        INSERT INTO tOrderStatusHistory(
            orderId
            , orderName
            , terminalId
            , truckId
            , createDtm
            , statusId
            , startStatusDtm
        )
        SELECT
            ord.id AS orderId
            , ord.name AS orderName
            , ord.terminalId
            , tot.truckId
            , ord.createDtm
            , osc.statusId
            , osc.startDtm AS startStatusDtm
        FROM tOrders AS ord
        INNER JOIN tOrderStatusCurrent AS osc
            ON osc.orderId = ord.id
        INNER JOIN tOrderTransporation AS tot
            ON tot.orderId = ord.id
        WHERE ord.id = param_orderId;
            
        DELETE osc
        FROM tOrderStatusCurrent AS osc
        WHERE osc.orderId = param_orderId;
        
        INSERT INTO tOrderStatusCurrent(
            orderId
            , statusId
        )
        VALUES (param_orderId, param_statusId);
    ELSE
        INSERT INTO tOrderStatusHistory(
            orderId
            , orderName
            , terminalId
            , truckId
            , createDtm
            , statusId
            , startStatusDtm
        )
        SELECT
            ord.id AS orderId
            , ord.name AS orderName
            , ord.terminalId
            , tot.truckId
            , ord.createDtm
            , osc.statusId
            , osc.startDtm AS startStatusDtm
        FROM tOrders AS ord
        INNER JOIN tOrderStatusCurrent AS osc
            ON osc.orderId = ord.id
        INNER JOIN tOrderTransporation AS tot
            ON tot.orderId = ord.id
        WHERE ord.id = param_orderId
        UNION
        SELECT
            ord.id AS orderId
            , ord.name AS orderName
            , ord.terminalId
            , tot.truckId
            , ord.createDtm
            , param_statusId
            , CURRENT_TIMESTAMP() AS startStatusDtm
        FROM tOrders AS ord
        INNER JOIN tOrderTransporation AS tot
            ON tot.orderId = ord.id
        WHERE ord.id = param_orderId;
        
        SET driverId = (
            SELECT
                tdr.userId
            FROM tDrivers AS tdr
            INNER JOIN tOrderTransporation AS tot
                ON tdr.truckId = tot.truckId
            WHERE tot.orderId = param_orderId
        ); 
            
        DELETE osc
        FROM tOrderStatusCurrent AS osc
        WHERE osc.orderId = param_orderId;
        
        DELETE ord
        FROM tOrders AS ord
        WHERE ord.id = param_orderId;
        
        SET orderCnt = (
            SELECT
                CASE
                    WHEN COUNT(tot.orderId) > 0
                    THEN COUNT(tot.orderId)
                    ELSE 0
                END AS orderCnt
            FROM tDrivers AS tdr
            INNER JOIN tOrderTransporation AS tot
                ON tot.truckId = tdr.truckId
        );
        
        IF orderCnt = 0 THEN
            UPDATE tDrivers
            SET statusId = 1
                , startDtm = CURRENT_TIMESTAMP()
            WHERE userId = driverId;
        END IF;
    
        
    END IF;
    
END //

