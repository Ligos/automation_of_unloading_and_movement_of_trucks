USE TRANSPORATION;

DROP PROCEDURE IF EXISTS prcDriversPlanning;

DELIMITER //

CREATE PROCEDURE prcDriversPlanning( IN param_driverId INTEGER )
BEGIN

    DECLARE dclTruckLicensePlate VARCHAR(16);
    DECLARE dclRestWeightKg FLOAT DEFAULT -1;
    DECLARE dclRestVolumeMet FLOAT DEFAULT -1;
    DECLARE dclCurrentCity VARCHAR(512);
    DECLARE dclCurrentTerminalType VARCHAR(128);

    DECLARE dclCurrentOrderId INTEGER;
    DECLARE dclCurrentWeightKg FLOAT;
    DECLARE dclCurrentVolumeMet FLOAT;

    DECLARE done INTEGER DEFAULT 0;
    DECLARE curPlan CURSOR FOR
        SELECT
            wor.orderId
            , ord.lengthMet * ord.widthMet * ord.heightMet AS volumeMet
            , wor.weightKg
        FROM vWeightOrderRank AS wor
        INNER JOIN tOrders AS ord
            ON ord.id = wor.orderId
        INNER JOIN vNonAssignedOrders AS nao
            ON nao.orderId = wor.orderId
        INNER JOIN tTerminals AS term
            ON term.id = ord.terminalId
        WHERE ord.cityDestination = dclCurrentCity
            AND term.type = dclCurrentTerminalType
        ORDER BY
            orderRank ASC;
    DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
    
    SET dclTruckLicensePlate = (
        SELECT
            vtr.licensePlateNumber
        FROM vDrivers AS vdr
        INNER JOIN vTrucks AS vtr
            ON vdr.truckId = vtr.truckId
        WHERE driverId = param_driverId
    );
    SET dclCurrentCity = ( -- текущий город по первому заказу
        SELECT DISTINCT
            ord.cityDestination
        FROM tOrderTransporation AS tot
        INNER JOIN tOrders AS ord
            ON ord.id = tot.orderId
        INNER JOIN vTrucks AS vtr
            ON vtr.truckId = tot.truckId
        WHERE vtr.licensePlateNumber = dclTruckLicensePlate
    );
    IF (dclCurrentCity IS NULL) THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'SQL: НАЗНАЧТЕ ПЕРВЫЙ ЗАКАЗ ВОДИТЕЛЮ/ГРУЗОВИКУ';
    END IF;
    SET dclCurrentTerminalType = (
        SELECT DISTINCT
            term.type
        FROM tOrderTransporation AS tot
        INNER JOIN tOrders AS ord
            ON ord.id = tot.orderId
        INNER JOIN vTrucks AS vtr
            ON vtr.truckId = tot.truckId
        INNER JOIN tTerminals AS term
            ON term.id = ord.terminalId
        WHERE vtr.licensePlateNumber = dclTruckLicensePlate
    );
    SET dclRestWeightKg = ( -- получаем свободный вес
        SELECT
            vtr.payLoadKg - ord.weightKg
        FROM vTrucks AS vtr
        LEFT JOIN(
            SELECT
                tot.truckId
                , SUM(ord.weightKg) AS weightKg
            FROM tOrderTransporation AS tot
            INNER JOIN tOrders AS ord
                ON ord.id = tot.orderId
            GROUP BY
                tot.truckId
        ) AS ord
            ON ord.truckId = vtr.truckId
        WHERE vtr.licensePlateNumber = dclTruckLicensePlate
    );
    SET dclRestVolumeMet = ( -- получаем свободный объём
        SELECT
            (vtr.lengthMet * vtr.heightMet * vtr.widthMet) - ord.volumeMet
        FROM vTrucks AS vtr
        LEFT JOIN(
            SELECT
                tot.truckId
                , SUM(ord.lengthMet * ord.heightMet * ord.widthMet) AS volumeMet
            FROM tOrderTransporation AS tot
            INNER JOIN tOrders AS ord
                ON ord.id = tot.orderId
            GROUP BY
                tot.truckId
        ) AS ord
            ON ord.truckId = vtr.truckId
        WHERE vtr.licensePlateNumber = dclTruckLicensePlate
    );
    
    IF (dclRestWeightKg <= 0 OR dclRestVolumeMet <= 0) THEN
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'SQL: ГРУЗОВИК ПЕРЕПОЛНЕН';
    END IF;
    
    OPEN curPlan;
    
    REPEAT

        FETCH curPlan INTO dclCurrentOrderId, dclCurrentVolumeMet, dclCurrentWeightKg;
        IF NOT done THEN
            IF (dclRestWeightKg - dclCurrentWeightKg > 0
                OR dclRestVolumeMet - dclCurrentVolumeMet > 0) THEN
            BEGIN
                SET dclRestWeightKg = dclRestWeightKg - dclCurrentWeightKg;
                SET dclRestVolumeMet = dclRestVolumeMet - dclCurrentVolumeMet;
                CALL prcAssignOrder(dclTruckLicensePlate, dclCurrentOrderId);
            END;
            END IF;
        END IF;
    UNTIL done END REPEAT;
    
    CLOSE curPlan;
    
END //


